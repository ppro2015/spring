package cz.uhk.ppro.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name="category")
public class Category {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id_category;

	@Size(min=3, max=20)
	@Column(name = "title", unique = true, nullable = false)
	private String title;

	@Size(min=3, max=20)
	@Column(name = "url", unique = true, nullable = true)
	private String url;

	@Size(min=1, max=500)
	@Column(name = "description", nullable = false)
	private String description;

	@ManyToOne(targetEntity = Category.class, optional = true, cascade = {CascadeType.ALL})
	@JoinColumn(name="id_parent")
	private Category id_parent;

	public int getId_category() {
		return id_category;
	}

	public void setId_category(int id_category) {
		this.id_category = id_category;
	}

	public String getTitle() {
		return title;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Category getId_parent() {
		return id_parent;
	}

	public void setId_parent(Category id_parent) {
		this.id_parent = id_parent;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + id_category;
		result = prime * result + ((id_parent == null) ? 0 : id_parent.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		result = prime * result + ((url == null) ? 0 : url.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Category other = (Category) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id_category != other.id_category)
			return false;
		if (id_parent == null) {
			if (other.id_parent != null)
				return false;
		} else if (!id_parent.equals(other.id_parent))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		if (url == null) {
			if (other.url != null)
				return false;
		} else if (!url.equals(other.url))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Category [id_category=" + id_category + ", title=" + title + ", url=" + url + ", description="
				+ description + ", id_parent=" + id_parent + "]";
	}

}