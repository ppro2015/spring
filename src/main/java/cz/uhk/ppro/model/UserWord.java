package cz.uhk.ppro.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

@Entity
@Table(name="user_word")
public class UserWord implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@ManyToOne
	@JoinColumn(name="id_user")
	private User user;

	@Id
	@ManyToOne
	@JoinColumn(name="id_word")
	private Word word;

	@Temporal(TemporalType.DATE)
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "next_repetition", nullable = false)
	private LocalDateTime next_repetition;

	@Column(name = "repetition_count", nullable = false)
	private int repetition_count;

	@Column(name = "repetition_correct", nullable = false)
	private int repetition_correct;

	@Column(name = "repetition_row", nullable = false)
	private int repetition_row;

	@Transient
	private String nextRepetitionString;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Word getWord() {
		return word;
	}

	public void setWord(Word word) {
		this.word = word;
	}

	public LocalDateTime getNext_repetition() {
		return next_repetition;
	}

	public void setNext_repetition(LocalDateTime next_repetition) {
		this.next_repetition = next_repetition;
	}

	public int getRepetition_count() {
		return repetition_count;
	}

	public void setRepetition_count(int repetition_count) {
		this.repetition_count = repetition_count;
	}

	public int getRepetition_correct() {
		return repetition_correct;
	}

	public void setRepetition_correct(int repetition_correct) {
		this.repetition_correct = repetition_correct;
	}

	public int getRepetition_row() {
		return repetition_row;
	}

	public void setRepetition_row(int repetition_row) {
		this.repetition_row = repetition_row;
	}

	public String getNextRepetitionString() {
		return nextRepetitionString;
	}

	public void setNextRepetitionString(String nextRepetitionString) {
		this.nextRepetitionString = nextRepetitionString;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((next_repetition == null) ? 0 : next_repetition.hashCode());
		result = prime * result + repetition_correct;
		result = prime * result + repetition_count;
		result = prime * result + repetition_row;
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		result = prime * result + ((word == null) ? 0 : word.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserWord other = (UserWord) obj;
		if (next_repetition == null) {
			if (other.next_repetition != null)
				return false;
		} else if (!next_repetition.equals(other.next_repetition))
			return false;
		if (repetition_correct != other.repetition_correct)
			return false;
		if (repetition_count != other.repetition_count)
			return false;
		if (repetition_row != other.repetition_row)
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		if (word == null) {
			if (other.word != null)
				return false;
		} else if (!word.equals(other.word))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "UserWord [user=" + user + ", word=" + word + ", next_repetition=" + next_repetition
				+ ", repetition_count=" + repetition_count + ", repetition_correct=" + repetition_correct
				+ ", repetition_row=" + repetition_row + "]";
	}

}
