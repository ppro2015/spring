package cz.uhk.ppro.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name="word")
public class Word {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id_word;

	@Size(min=1, max=150)
	@Column(name = "key_word", nullable = false)
	private String key;

	@Size(min=1, max=50)
	@Column(name = "answer", nullable = false)
	private String answer;

	@ManyToOne
	@JoinColumn(name="id_test")
	private Test test;

	public int getId_word() {
		return id_word;
	}

	public void setId_word(int id_word) {
		this.id_word = id_word;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public Test getTest() {
		return test;
	}

	public void setTest(Test test) {
		this.test = test;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((answer == null) ? 0 : answer.hashCode());
		result = prime * result + id_word;
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		result = prime * result + ((test == null) ? 0 : test.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Word other = (Word) obj;
		if (answer == null) {
			if (other.answer != null)
				return false;
		} else if (!answer.equals(other.answer))
			return false;
		if (id_word != other.id_word)
			return false;
		if (key == null) {
			if (other.key != null)
				return false;
		} else if (!key.equals(other.key))
			return false;
		if (test == null) {
			if (other.test != null)
				return false;
		} else if (!test.equals(other.test))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Word [id_word=" + id_word + ", key=" + key + ", answer=" + answer + ", test=" + test + "]";
	}

}