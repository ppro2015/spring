package cz.uhk.ppro.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
@Table(name="user")
public class User {

	// validation group marker interfaces
	public interface ValidationCreateUser { }
	public interface ValidationUpdateUser { }
	public interface ValidationPassword { }
	public interface ValidationDailyGoal { }

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id_user;

	@Size(min=3, max=20, groups = {ValidationUpdateUser.class, ValidationCreateUser.class})
	@Pattern(regexp="^[\\S]*$", groups = {ValidationUpdateUser.class, ValidationCreateUser.class})
	@Column(name = "nickname", unique = true, nullable = false)
	private String nickname;

	@NotNull(groups = {ValidationPassword.class})
	@Size.List ({
		@Size(min=6, message="The password must be at least {min} characters long.",
				groups = {ValidationPassword.class, ValidationCreateUser.class}),
		@Size(max=60, message="The password can not be longer than {max} characters.",
				groups = {ValidationPassword.class, ValidationCreateUser.class})
	})
	@Column(name = "password", nullable = false)
	private String password;

	@Transient
	private String confirmPassword;

	public enum Role {
		 ROLE_USER, ROLE_ADMIN, ROLE_SUPERUSER;
	}

	@Column(name = "role", nullable = false)
	@Enumerated(EnumType.STRING)
	private Role role;

	@NotNull(groups = {ValidationUpdateUser.class})
	@Column(name = "registration_date", columnDefinition="DATETIME", nullable = false, updatable = false)
	private Date registration_date = new Date();

	@Min(value=1, groups = {ValidationUpdateUser.class, ValidationDailyGoal.class})
	@NotNull(groups = {ValidationUpdateUser.class, ValidationDailyGoal.class})
	@Column(name = "daily_goal", nullable = false)
	private Integer daily_goal;

	@Min(value=0, groups = {ValidationUpdateUser.class})
	@NotNull(groups = {ValidationUpdateUser.class})
	@Column(name = "daily_done", nullable = false)
	private Integer daily_done;

	@NotNull
	@Column(name = "last_update_daily_done", columnDefinition="DATETIME", nullable = false)
	private Date last_update_daily_done = new Date();

	@Min(value=0, groups = {ValidationUpdateUser.class})
	@NotNull(groups = {ValidationUpdateUser.class})
	@Column(name = "points", nullable = false)
	private Integer points;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
	private Set<UserTest> userTests;

	public int getId_user() {
		return id_user;
	}

	public void setId_user(int id_user) {
		this.id_user = id_user;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Date getRegistration_date() {
		return registration_date;
	}

	public void setRegistration_date(Date registration_date) {
		this.registration_date = registration_date;
	}

	public Integer getDaily_goal() {
		return daily_goal;
	}

	public void setDaily_goal(Integer daily_goal) {
		this.daily_goal = daily_goal;
	}

	public Integer getDaily_done() {
		return daily_done;
	}

	public void setDaily_done(Integer daily_done) {
		this.daily_done = daily_done;
	}

	public Date getLast_update_daily_done() {
		return last_update_daily_done;
	}

	public void setLast_update_daily_done(Date last_update_daily_done) {
		this.last_update_daily_done = last_update_daily_done;
	}

	public Integer getPoints() {
		return points;
	}

	public void setPoints(Integer points) {
		this.points = points;
	}

	public Set<UserTest> getUserTests() {
		return userTests;
	}

	public void setUserTests(Set<UserTest> userTests) {
		this.userTests = userTests;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((confirmPassword == null) ? 0 : confirmPassword.hashCode());
		result = prime * result + ((daily_done == null) ? 0 : daily_done.hashCode());
		result = prime * result + ((daily_goal == null) ? 0 : daily_goal.hashCode());
		result = prime * result + id_user;
		result = prime * result + ((nickname == null) ? 0 : nickname.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((points == null) ? 0 : points.hashCode());
		result = prime * result + ((registration_date == null) ? 0 : registration_date.hashCode());
		result = prime * result + ((role == null) ? 0 : role.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (confirmPassword == null) {
			if (other.confirmPassword != null)
				return false;
		} else if (!confirmPassword.equals(other.confirmPassword))
			return false;
		if (daily_done == null) {
			if (other.daily_done != null)
				return false;
		} else if (!daily_done.equals(other.daily_done))
			return false;
		if (daily_goal == null) {
			if (other.daily_goal != null)
				return false;
		} else if (!daily_goal.equals(other.daily_goal))
			return false;
		if (id_user != other.id_user)
			return false;
		if (nickname == null) {
			if (other.nickname != null)
				return false;
		} else if (!nickname.equals(other.nickname))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (points == null) {
			if (other.points != null)
				return false;
		} else if (!points.equals(other.points))
			return false;
		if (registration_date == null) {
			if (other.registration_date != null)
				return false;
		} else if (!registration_date.equals(other.registration_date))
			return false;
		if (role != other.role)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "User [id_user=" + id_user + ", nickname=" + nickname + ", password=" + password + ", confirmPassword="
				+ confirmPassword + ", role=" + role + ", registration_date=" + registration_date + ", daily_goal="
				+ daily_goal + ", daily_done=" + daily_done + ", last_update_daily_done=" + last_update_daily_done
				+ ", points=" + points + ", userTests=" + userTests + "]";
	}

}