package cz.uhk.ppro.service;

import java.util.List;

import cz.uhk.ppro.model.Test;
import cz.uhk.ppro.model.User;
import cz.uhk.ppro.model.UserTest;
import cz.uhk.ppro.model.UserWord;
import cz.uhk.ppro.model.Word;

public interface UserTestService {

	/**
	 * Finds {@link UserTest} by {@link User} and {@link Test}
	 * @param user
	 * @param test
	 * @return {@link UserTest}
	 */
	UserTest findUserTestByUserAndTest(User user, Test test);

	/**
	 * Finds out if user is in test
	 * @param user
	 * @param test
	 * @return return true if user is in test, false otherwise
	 */
	boolean isUserInTest(User user, Test test);

	/**
	 * Finds all {@link UserTest}s by {@link User}
	 * @param user
	 * @return {@link List} of {@link UserTest}
	 */
	List<UserTest> findAllUserTestByUser(User user);

	/**
	 * Finds all {@link UserTest}s by {@link Test}
	 * @param test
	 * @return {@link List} of {@link UserTest}
	 */
	List<UserTest> findAllUserTestsByTest(Test test);

	/**
	 * Adds {@link User} to {@link Test}
	 * @param user
	 * @param test
	 */
	void addUserToTest(User user, Test test);

	/**
	 * Updates {@link User} in {@link Test}
	 * @param userTest
	 */
	void updateUserTest(UserTest userTest);

	/**
	 * Deletes {@link User} from {@link Test}
	 * @param userTest {@link UserTest}
	 */
	void deleteUserTest(UserTest userTest);

	/**
	 * Deletes {@link List} of {@link UserTest}s
	 * @param userTests
	 */
	void deleteUserTests(List<UserTest> userTests);

	/**
	 * Gives {@link User} points depending on his row in given {@link Word}
	 * @param user
	 * @param test
	 * @param reviewWord
	 * @return number of points
	 */
	int addPoints(User user, Test test, UserWord reviewWord);

}