package cz.uhk.ppro.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cz.uhk.ppro.dao.UserTestDao;
import cz.uhk.ppro.model.Test;
import cz.uhk.ppro.model.User;
import cz.uhk.ppro.model.UserTest;
import cz.uhk.ppro.model.UserWord;

@Service("userTestService")
@Transactional
public class UserTestServiceImpl implements UserTestService {

	@Autowired
	private UserTestDao dao;

	@Override
	public UserTest findUserTestByUserAndTest(User user, Test test) {
		return dao.findUserTestByUserAndTest(user, test);
	}

	@Override
	public boolean isUserInTest(User user, Test test) {
		UserTest userTest = findUserTestByUserAndTest(user, test);
		return userTest != null;
	}

	@Override
	public List<UserTest> findAllUserTestByUser(User user) {
		return dao.findAllUserTestByUser(user);
	}

	@Override
	public List<UserTest> findAllUserTestsByTest(Test test) {
		return dao.findAllUserTestsByTest(test);
	}

	@Override
	public void addUserToTest(User user, Test test) {
		dao.addUserToTest(user, test);
	}

	@Override
	public void updateUserTest(UserTest userTest) {
		UserTest entity = findUserTestByUserAndTest(userTest.getUser(), userTest.getTest());
		if (entity != null) {
			entity.setStart_date(userTest.getStart_date());
			entity.setPoints(userTest.getPoints());
		}
	}

	@Override
	public void deleteUserTest(UserTest userTest) {
		dao.deleteUserTest(userTest);
	}

	@Override
	public void deleteUserTests(List<UserTest> userTests) {
		dao.deleteUserTests(userTests);
	}

	@Override
	public int addPoints(User user, Test test, UserWord reviewWord) {
		int row = reviewWord.getRepetition_row();
		int points = 0;
		if (row <= 1) points = 10;
		else if (row >= 10) points = 100;
		else points = row * 10;

		UserTest userTest = findUserTestByUserAndTest(user, test);
		userTest.setPoints(userTest.getPoints() + points);
		updateUserTest(userTest);
		return points;
	}

}