package cz.uhk.ppro.service;

import java.util.List;

import cz.uhk.ppro.model.Category;

/**
 * Service class for {@link Category} class
 */
public interface CategoryService {

	/**
	 * Returns {@link Category} by its id
	 * @param id
	 * @return {@link Category}
	 */
	Category findCategoryById(int id);

	/**
	 * Saves new category to database
	 * @param category instance of category to save
	 */
	void saveCategory(Category category);

	/**
	 * Updates {@link Category} in database
	 * @param category instance of category that holds new data
	 */
	void updateCategory(Category category);

	/**
	 * Deletes {@link Category} from database
	 * @param category instance of category to delete
	 */
	void deleteCategory(Category category);

	/**
	 * Finds all {@link Category}es
	 * @return {@link Category} of all {@link Category}es
	 */
	List<Category> findAllCategories(); 

	/**
	 * Finds {@link Category} by title 
	 * @param title title of category
	 * @return {@link Category}
	 */
	Category findCategoryByTitle(String title);

	/**
	 * Finds {@link Category} by url
	 * @param url url of category
	 * @return {@link Category}
	 */
	Category findCategoryByUrl(String url);

	/**
	 * Finds all {@link Category}es with no parent (they are potentially parent categories) 
	 * @return {@link List} of all {@link Category}es that have null parent
	 */
	List<Category> findAllNonParentCategories();

	/**
	 * Returns true when {@link Category} title is unique
	 * @param id
	 * @param title
	 * @return true if title is unique, false otherwise
	 */
	boolean isCategoryTitleUnique(Integer id, String title);

	/**
	 * Returns true when {@link Category} url is unique
	 * @param id
	 * @param url
	 * @return true if url is unique, false otherwise
	 */
	boolean isCategoryUrlUnique(Integer id, String url);

	/**
	 * Returns true when {@link Category} is parent
	 * @param id_parent
	 * @return true if category is parent, false otherwise
	 */
	boolean isCategoryParent(Category id_parent);

}