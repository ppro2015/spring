package cz.uhk.ppro.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cz.uhk.ppro.common.Utils;
import cz.uhk.ppro.dao.CategoryDao;
import cz.uhk.ppro.model.Category;

@Service("categoryService")
@Transactional
public class CategoryServiceImpl implements CategoryService {

	@Autowired
	private CategoryDao dao;

	@Override
	public Category findCategoryById(int id) {
		return dao.findById(id);
	}

	@Override
	public void saveCategory(Category category) {
		category.setUrl(Utils.urlifyString(category.getTitle()));
		if (category.getId_parent().getId_category() <= 0) {
			category.setId_parent(null);		
		} else {
			category.setId_parent(findCategoryById(category.getId_parent().getId_category()));	
		}
		dao.saveCategory(category);
	}

	@Override
	public void updateCategory(Category category) {
		Category entity = dao.findById(category.getId_category());
		Category parent = null;
		if (category.getId_parent().getId_category() != 0) {
			parent = dao.findById(category.getId_parent().getId_category());	
		}
		if (entity != null) {
			entity.setTitle(category.getTitle());
			entity.setUrl(Utils.urlifyString(category.getTitle()));
			entity.setDescription(category.getDescription());
			entity.setId_parent(parent);
		}
	}

	@Override
	public void deleteCategory(Category category) {
		Category entity = findCategoryById(category.getId_category());
		entity.setId_parent(null);
		dao.deleteCategory(entity);
	}

	@Override
	public List<Category> findAllCategories() {
		return dao.findAllCategories();
	}

	@Override
	public Category findCategoryByTitle(String title) {
		return dao.findCategoryByTitle(title);
	}

	@Override
	public Category findCategoryByUrl(String url) {
		return dao.findCategoryByUrl(url);
	}

	@Override
	public boolean isCategoryTitleUnique(Integer id, String title) {
		Category category = findCategoryByTitle(title);
		return ( category == null || ((id != null) && (category.getId_category() == id)) );
	}

	@Override
	public boolean isCategoryUrlUnique(Integer id, String url) {
		Category category = findCategoryByUrl(url);
		return ( category == null || ((id != null) && (category.getId_category() == id)) );
	}

	@Override
	public List<Category> findAllNonParentCategories() {
		return dao.findAllNonParentCategories();
	}

	@Override
	public boolean isCategoryParent(Category id_parent) {
		if (id_parent.getId_category() == 0) {
			return true;
		}
		return findCategoryById(id_parent.getId_category()).getId_parent() == null;
	}

}