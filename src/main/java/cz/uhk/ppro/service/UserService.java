package cz.uhk.ppro.service;

import java.util.List;

import cz.uhk.ppro.model.User;
import cz.uhk.ppro.model.UserTest;

/**
 * Service class for {@link User} class
 */
public interface UserService {

	/**
	 * Returns {@link User} by its id
	 * @param id id of user to find 
	 * @return {@link User}
	 */
	User findUserById(int id);

	/**
	 * Finds {@link User} by nickname
	 * @param nickname nickname of user to find 
	 * @return {@link User}
	 */
	User findUserByNickname(String nickname);

	/**
	 * Saves new user to database
	 * @param user instance of test to save
	 */
	void saveUser(User user);

	/**
	 * Updates {@link User} in database
	 * @param user instance of user that holds new data
	 */
	void updateUser(User user);

	/**
	 * Updates {@link User} password in database
	 * @param user instance of user that holds new data
	 */
	void updateUserPassword(User user);

	/**
	 * Deletes {@link User} from database
	 * @param user instance of user to delete
	 */
	void deleteUser(User user);

	/**
	 * Finds all {@link User}s
	 * @return {@link List} of all {@link User}s
	 */
	List<User> findAllUsers();

	/**
	 * Finds all {@link User}s with initialized {@link UserTest}s
	 * @return {@link List} of all {@link User}s
	 */
	List<User> findAllUsersWithUserTests();

	/**
	 * Returns true when users' nickname is unique
	 * @param id
	 * @param nickname
	 * @return true if nickname is unique, false otherwise
	 */
	boolean isUserNicknameUnique(Integer id, String nickname);

	/**
	 * Updates {@link User}'s daily done count of points
	 * @param user {@link User}
	 */
	void updateUserDailyDone(User user);

}