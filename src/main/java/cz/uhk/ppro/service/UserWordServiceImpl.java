package cz.uhk.ppro.service;

import java.util.List;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cz.uhk.ppro.common.CommonConstants;
import cz.uhk.ppro.dao.UserWordDao;
import cz.uhk.ppro.dto.CheckedItem;
import cz.uhk.ppro.model.Test;
import cz.uhk.ppro.model.User;
import cz.uhk.ppro.model.UserWord;
import cz.uhk.ppro.model.Word;

@Service("userWordService")
@Transactional
public class UserWordServiceImpl implements UserWordService {

	@Autowired
	private UserWordDao dao;

	@Override
	public List<UserWord> findAllUserWordsByUser(User user) {
		return dao.findAllUserWordsByUser(user);
	}

	@Override
	public List<UserWord> findAllUserWordsForReview(User user, Test test) {
		return dao.findAllUserWordsForReview(user, test);
	}

	@Override
	public List<UserWord> findAllUserWordsByWord(Word word) {
		return dao.findAllUserWordsByWord(word);
	}

	@Override
	public List<UserWord> findAllUserWordsByUserAndWords(User user, List<Word> words) {
		return dao.findAllUserWordsByUserAndWords(user, words);
	}

	@Override
	public UserWord findUserWordByUserAndWord(User user, Word word) {
		return dao.findUserWordByUserAndWord(user, word);
	}

	@Override
	public void saveWordForUsers(Word word, List<User> users) {
		dao.saveWordForUsers(word, users);
	}

	@Override
	public void saveWordsForUser(User user, List<Word> words) {
		dao.saveWordsForUser(user, words);
	}

	@Override
	public void updateUserWord(UserWord userWord) {
		UserWord entity = findUserWordByUserAndWord(userWord.getUser(), userWord.getWord());
		if (entity != null) {
			entity.setNext_repetition(userWord.getNext_repetition());
			entity.setRepetition_correct(userWord.getRepetition_correct());
			entity.setRepetition_count(userWord.getRepetition_count());
			entity.setRepetition_row(userWord.getRepetition_row());
		}
	}

	@Override
	public void deleteUserWords(List<UserWord> userWords) {
		dao.deleteUserWords(userWords);
	}

	@Override
	public CheckedItem checkWord(UserWord reviewWord, String answer) {
		CheckedItem checkedItem = new CheckedItem();
		checkedItem.setCorrectAnswer(reviewWord.getWord().getAnswer());
		checkedItem.setAnswer(answer);

		if (reviewWord.getWord().getAnswer().equalsIgnoreCase(answer)) {
			checkedItem.setCorrect(true);

			//update row
			int row = reviewWord.getRepetition_row();
			if (row >= 0) {
				row++;
			} else {
				row = 1;
			}
			reviewWord.setRepetition_row(row);
			reviewWord.setRepetition_count(reviewWord.getRepetition_count() + 1);
			reviewWord.setRepetition_correct(reviewWord.getRepetition_correct() + 1);

			// count next repetition
			int plusHours = ((int) (Math.pow(3, row - 1))) * CommonConstants.REPETITION_BASE_HOUR;
			LocalDateTime ldt = new LocalDateTime().plusHours(plusHours);
			reviewWord.setNext_repetition(ldt);
		} else {
			checkedItem.setCorrect(false);
			//update row
			int row = reviewWord.getRepetition_row();
			if (row <= 0) {
				row--;
			} else {
				row = -1;
			}
			reviewWord.setRepetition_row(row);
			reviewWord.setRepetition_count(reviewWord.getRepetition_count() + 1);

			// count next repetition
			LocalDateTime ldt = new LocalDateTime().plusHours(CommonConstants.REPETITION_BASE_HOUR);
			reviewWord.setNext_repetition(ldt);
		}
		checkedItem.setRepetition_correct(reviewWord.getRepetition_correct());
		checkedItem.setRepetition_count(reviewWord.getRepetition_count());
		checkedItem.setRepetition_row(reviewWord.getRepetition_row());

		updateUserWord(reviewWord);
		return checkedItem;
	}

}