package cz.uhk.ppro.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cz.uhk.ppro.common.CommonConstants;
import cz.uhk.ppro.dao.UserDao;
import cz.uhk.ppro.model.User;

@Service("userService")
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao dao;

	@Override
	public User findUserById(int id) {
		return dao.findById(id);
	}

	@Override
	public User findUserByNickname(String nickname) {
		return dao.findUserByNickname(nickname);
	}

	@Override
	public void saveUser(User user) {
		dao.createUser(user);
	}

	@Override
	public void updateUser(User user) {
		User entity = dao.findById(user.getId_user());
		if (entity != null) {
			entity.setNickname(user.getNickname());
			entity.setRole(user.getRole());
			entity.setDaily_done(user.getDaily_done());
			entity.setDaily_goal(user.getDaily_goal());
			entity.setPoints(user.getPoints());
		}
	}

	@Override
	public void updateUserPassword(User user) {
		User entity = dao.findById(user.getId_user());
		if (entity != null) {
			String pw_hash = BCrypt.hashpw(user.getPassword(), BCrypt.gensalt(CommonConstants.BCRYPT_STRENGTH));
			entity.setPassword(pw_hash);
		}
	}

	@Override
	public void deleteUser(User user) {
		dao.deleteUser(user);
	}

	@Override
	public List<User> findAllUsers() {
		return dao.findAllUsers();
	}

	@Override
	public List<User> findAllUsersWithUserTests() {
		return dao.findAllUsersWithUserTests();
	}

	@Override
	public boolean isUserNicknameUnique(Integer id, String nickname) {
		User user = findUserByNickname(nickname);
		return ( user == null || ((id != null) && (user.getId_user() == id)) );
	}

	@Override
	public void updateUserDailyDone(User user) {
		dao.updateUserDailyDone(user);
	}

}