package cz.uhk.ppro.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cz.uhk.ppro.dao.WordDao;
import cz.uhk.ppro.model.Test;
import cz.uhk.ppro.model.Word;

@Service("wordService")
@Transactional
public class WordServiceImpl implements WordService {

	@Autowired
	private WordDao dao;

	@Override
	public Word findWordById(int id) {
		return dao.findWordById(id);
	}

	@Override
	public void saveWord(Word word) {
		dao.saveWord(word);
	}

	@Override
	public void updateWord(Word word) {
		Word entity = findWordById(word.getId_word());
		if (entity != null) {
			entity.setKey(word.getKey());
			entity.setAnswer(word.getAnswer());
		}
	}

	@Override
	public void deleteWord(Word word) {
		Word entity = findWordById(word.getId_word());
		dao.deleteWord(entity);
	}

	@Override
	public List<Word> findAllWords() {
		return dao.findAllWords();
	}

	@Override
	public List<Word> findAllWordsInTest(Test test) {
		return dao.findAllWordsInTest(test);
	}

}