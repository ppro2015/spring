package cz.uhk.ppro.service;

import java.util.List;

import cz.uhk.ppro.dto.CheckedItem;
import cz.uhk.ppro.model.Test;
import cz.uhk.ppro.model.User;
import cz.uhk.ppro.model.UserWord;
import cz.uhk.ppro.model.Word;

/**
 * Service class for {@link UserWord} class
 */
public interface UserWordService {

	/**
	 * Finds all {@link UserWord}s by {@link User}
	 * - meaning all {@link User}s' {@link UserWord}s
	 * @param user 
	 * @return {@link List} of {@link UserWord}
	 */
	List<UserWord> findAllUserWordsByUser(User user);

	/**
	 * Finds all {@link UserWord}s by {@link User} and {@link Test}
	 * - meaning all {@link User}s' {@link UserWord}s for given {@link Test}
	 * @param user
	 * @param test
	 * @return {@link List} of {@link UserWord}
	 */
	List<UserWord> findAllUserWordsForReview(User user, Test test);

	/**
	 * Finds all {@link UserWord}s by id_word
	 * - meaning all {@link Word}s' {@link UserWord}s
	 * @param id_word id of word
	 * @return {@link List} of {@link UserWord}
	 */
	List<UserWord> findAllUserWordsByWord(Word word);

	/**
	 * Finds all {@link UserWord}s by id_user and words
	 * - meaning all {@link UserWord}s for given {@link User} and list of {@link Word}s
	 * (used for deleting of test)
	 * @param user
	 * @param words {@link List} of {@link Word}s
	 * @return {@link List} of {@link UserWord}
	 */
	List<UserWord> findAllUserWordsByUserAndWords(User user, List<Word> words);

	/**
	 * Finds one {@link UserWord} by {@link User} and {@link Word}
	 * @param user
	 * @param word
	 * @return {@link UserWord}
	 */
	UserWord findUserWordByUserAndWord(User user, Word word);

	/**
	 * Creates {@link UserWord}s for given {@link Word} and {@link List} of {@link User}s
	 * - used when creating new word
	 * @param word instance of {@link Word}
	 */
	void saveWordForUsers(Word word, List<User> users);

	/**
	 * Creates {@link UserWord}s for given {@link User} and {@link List} of {@link Word}s
	 * - used when adding user to test
	 * @param user instance of {@link User}
	 * @param words {@link List} of {@link Word}s
	 */
	void saveWordsForUser(User user, List<Word> words);

	/**
	 * Updates {@link UserWord} in database
	 * @param userWord instance of userWord that holds new data
	 */
	void updateUserWord(UserWord userWord);

	/**
	 * Deletes given {@link List} of {@link UserWord}s
	 * @param userWords {@link List} of {@link UserWord}s
	 */
	void deleteUserWords(List<UserWord> userWords);

	/**
	 * Checks answered word
	 * @param reviewWord reviewing {@link UserWord}
	 * @param answer inserted answer
	 * @return {@link CheckedItem}
	 */
	CheckedItem checkWord(UserWord reviewWord, String answer);

}