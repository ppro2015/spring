package cz.uhk.ppro.service;

import java.util.List;

import cz.uhk.ppro.dto.CourseWithCount;
import cz.uhk.ppro.model.Category;
import cz.uhk.ppro.model.Test;

/**
 * Service class for {@link Test} class
 */
public interface TestService {

	/**
	 * Returns {@link Test} by its id
	 * @param id
	 * @return Test
	 */
	Test findTestById(int id);

	/**
	 * Saves new test to database
	 * @param test instance of test to save
	 */
	void saveTest(Test test);

	/**
	 * Updates {@link Test} in database
	 * @param test instance of test that holds new data
	 */
	void updateTest(Test test);

	/**
	 * Deletes {@link Test} from database
	 * @param test instance of test to delete
	 */
	void deleteTest(Test test);

	/**
	 * Finds all {@link Test}s
	 * @return {@link List} of all {@link Test}s
	 */
	List<Test> findAllTests();

	/**
	 * Finds all {@link Test}s in category
	 * @param category
	 * @return {@link List} of {@link Test}s
	 */
	List<Test> findAllTestsByCategory(Category category);

	/**
	 * Finds all {@link Test}s in category by title
	 * @param title
	 * @return {@link List} of all {@link Test}s
	 */
	List<Test> findAllTestsByCategoryTitle(String title);

	/**
	 * Finds all {@link Test}s with count of users studying test
	 * @return {@link List} of {@link CourseWithCount}
	 */
	List<CourseWithCount> findAllTestsWithUserCounts();

	/**
	 * Finds all {@link Test}s by {@link Category} with count of users studying test
	 * @param category
	 * @return {@link List} of {@link CourseWithCount}
	 */
	List<CourseWithCount> findAllTestsByCategoryWithUserCounts(Category category);

}