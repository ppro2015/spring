package cz.uhk.ppro.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cz.uhk.ppro.dao.TestDao;
import cz.uhk.ppro.dto.CourseWithCount;
import cz.uhk.ppro.model.Category;
import cz.uhk.ppro.model.Test;

@Service("testService")
@Transactional
public class TestServiceImpl implements TestService {

	@Autowired
	private TestDao dao;

	@Override
	public Test findTestById(int id) {
		return dao.findTestById(id);
	}

	@Override
	public void saveTest(Test test) {
		dao.saveTest(test);
	}

	@Override
	public void updateTest(Test test) {
		Test entity = findTestById(test.getId_test());
		if (entity != null) {
			entity.setTitle(test.getTitle());
			entity.setDescription(test.getDescription());
			entity.setPicture(test.getPicture());
			entity.setAuthor(test.getAuthor());
			entity.setCategory(test.getCategory());
		}
	}

	@Override
	public void deleteTest(Test test) {
		dao.deleteTest(test);
	}

	@Override
	public List<Test> findAllTests() {
		return dao.findAllTests();
	}

	@Override
	public List<Test> findAllTestsByCategory(Category category) {
		return dao.findAllTestsByCategory(category);
	}
	@Override
	public List<Test> findAllTestsByCategoryTitle(String title) {
		return dao.findAllTestsByCategoryTitle(title);
	}

	@Override
	public List<CourseWithCount> findAllTestsWithUserCounts() {
		return dao.findAllTestsWithUserCounts();
	}

	@Override
	public List<CourseWithCount> findAllTestsByCategoryWithUserCounts(Category category) {
		return dao.findAllTestsByCategoryWithUserCounts(category);
	}
}