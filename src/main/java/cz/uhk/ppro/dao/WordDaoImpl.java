package cz.uhk.ppro.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import cz.uhk.ppro.model.Test;
import cz.uhk.ppro.model.Word;

@Repository("wordDao")
public class WordDaoImpl extends AbstractDao<Integer, Word> implements WordDao {

	@Override
	public Word findWordById(int id) {
		return getByKey(id);
	}

	@Override
	public void saveWord(Word word) {
		persist(word);
	}

	@Override
	public void deleteWord(Word word) {
		delete(word);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Word> findAllWords() {
		Criteria criteria = createEntityCriteria().addOrder(Order.asc("id_word"));
		return (List<Word>) criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Word> findAllWordsInTest(Test test) {
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("test", test));
		return (List<Word>) criteria.list();
	}

}