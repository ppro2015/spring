package cz.uhk.ppro.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.stereotype.Repository;

import cz.uhk.ppro.dto.CourseWithCount;
import cz.uhk.ppro.model.Category;
import cz.uhk.ppro.model.Test;
import cz.uhk.ppro.model.UserTest;

@Repository("testDao")
public class TestDaoImpl extends AbstractDao<Integer, Test> implements TestDao {

	@Override
	public Test findTestById(int id) {
		return getByKey(id);
	}

	@Override
	public void saveTest(Test test) {
		persist(test);
	}

	@Override
	public void deleteTest(Test test) {
		delete(test);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Test> findAllTests() {
		Criteria criteria = createEntityCriteria().addOrder(Order.asc("id_test"));
		List<Test> tests = (List<Test>) criteria.list();
		for (Test test : tests) {
			Hibernate.initialize(test.getUserTests());
		}
		return tests;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Test> findAllTestsByCategory(Category category) {
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("category", category));
		List<Test> tests = (List<Test>) criteria.list();
		return tests;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Test> findAllTestsByCategoryTitle(String title) {
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("category.title", title));
		List<Test> tests = (List<Test>) criteria.list();
		return tests;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CourseWithCount> findAllTestsWithUserCounts() {
		Criteria criteria = prepareBasicCriteriaForAllTestsWithUserCounts();
		List<Object[]> testObjects = (List<Object[]>) criteria.list();
		return mapToCourse(testObjects);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CourseWithCount> findAllTestsByCategoryWithUserCounts(Category category) {
		Criteria criteria = prepareBasicCriteriaForAllTestsWithUserCounts();
		criteria.add(Restrictions.eq("test.category", category));
		List<Object[]> testObjects = (List<Object[]>) criteria.list();
		return mapToCourse(testObjects);
	}

	private List<CourseWithCount> mapToCourse(List<Object[]> testObjects) {
		List<CourseWithCount> courses = new ArrayList<>(testObjects.size());
		for (Object[] testObject : testObjects) {
			courses.add(new CourseWithCount(testObject));
		}
		return courses;
	}

	private Criteria prepareBasicCriteriaForAllTestsWithUserCounts() {
		Criteria criteria = getSession().createCriteria(UserTest.class, "ut")
				.createCriteria("ut.test", "test", JoinType.RIGHT_OUTER_JOIN)
				.addOrder(Order.asc("id_test"));

		ProjectionList projectionList = Projections.projectionList();
		projectionList.add(Projections.property("test.id_test"));
		projectionList.add(Projections.property("test.title"));
		projectionList.add(Projections.property("test.picture"));
		projectionList.add(Projections.property("test.author"));
		projectionList.add(Projections.property("test.category"));
		projectionList
			.add(Projections.groupProperty("ut.test.id_test"))
			.add(Projections.groupProperty("test.id_test"))
			.add(Projections.alias(Projections.count("ut.test.id_test"), "userCount"));
		criteria.setProjection(projectionList);
		return criteria;
	}
}