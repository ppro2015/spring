package cz.uhk.ppro.dao;

import java.util.List;

import cz.uhk.ppro.model.Test;
import cz.uhk.ppro.model.User;
import cz.uhk.ppro.model.UserTest;

public interface UserTestDao {

	/**
	 * Finds {@link UserTest} by {@link User} and {@link Test}
	 * @param user
	 * @param test
	 * @return {@link UserTest}
	 */
	UserTest findUserTestByUserAndTest(User user, Test test);

	/**
	 * Finds all {@link UserTest}s by {@link User}
	 * @param user
	 * @return {@link List} of {@link UserTest}
	 */
	List<UserTest> findAllUserTestByUser(User user);

	/**
	 * Finds all {@link UserTest}s by {@link Test}
	 * @param test
	 * @return {@link List} of {@link UserTest}
	 */
	List<UserTest> findAllUserTestsByTest(Test test);

	/**
	 * Adds {@link User} to {@link Test}
	 * @param user
	 * @param test
	 */
	void addUserToTest(User user, Test test);

	/**
	 * Deletes {@link User} from {@link Test}
	 * @param userTest {@link UserTest}
	 */
	void deleteUserTest(UserTest userTest);

	/**
	 * Deletes {@link List} of {@link UserTest}s
	 * @param userTests
	 */
	void deleteUserTests(List<UserTest> userTests);

}