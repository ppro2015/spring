package cz.uhk.ppro.dao;

import java.util.List;

import cz.uhk.ppro.model.Test;
import cz.uhk.ppro.model.Word;

/**
 * DAO class for {@link Word} class
 */
public interface WordDao {

	/**
	 * Returns {@link Word} by its id
	 * @param id
	 * @return Word
	 */
	Word findWordById(int id);

	/**
	 * Saves new word to database
	 * @param word instance of word to save
	 */
	void saveWord(Word word);

	/**
	 * Deletes {@link Word} from database
	 * @param word instance of word to delete
	 */
	void deleteWord(Word word);

	/**
	 * Finds all {@link Word}s
	 * @return {@link List} of all {@link Word}s
	 */
	List<Word> findAllWords();

	/**
	 * Finds all {@link Word}s in test by id of test
	 * @param test test by which to find words
	 * @return {@link List} of all {@link Word}s in test
	 */
	List<Word> findAllWordsInTest(Test test);

}