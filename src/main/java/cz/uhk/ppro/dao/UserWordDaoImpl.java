package cz.uhk.ppro.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.joda.time.LocalDateTime;
import org.springframework.stereotype.Repository;

import cz.uhk.ppro.model.Test;
import cz.uhk.ppro.model.User;
import cz.uhk.ppro.model.UserWord;
import cz.uhk.ppro.model.Word;

@Repository("userWordDao")
public class UserWordDaoImpl extends AbstractDao<Integer, UserWord> implements UserWordDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<UserWord> findAllUserWordsByUser(User user) {
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("user", user));
		List<UserWord> userWords = (List<UserWord>) criteria.list();
		return userWords;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UserWord> findAllUserWordsForReview(User user, Test test) {
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("user", user));
		criteria.createAlias("word.test", "wt").add(Restrictions.eq("wt.id_test", test.getId_test()));//not possible to use nested paths directly in Criteria API
		criteria.add(Restrictions.lt("next_repetition", new LocalDateTime()));

		List<UserWord> userWords = (List<UserWord>) criteria.list();
		return userWords;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UserWord> findAllUserWordsByWord(Word word) {
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("word", word));
		List<UserWord> userWords = (List<UserWord>) criteria.list();
		return userWords;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UserWord> findAllUserWordsByUserAndWords(User user, List<Word> words) {
		if (words.size() > 0) {
			Integer [] wordsId = new Integer[words.size()];
			for (int i = 0; i < wordsId.length; i++) {
				wordsId[i] = words.get(i).getId_word();
			}
			Criteria criteria = createEntityCriteria();
			criteria.add(Restrictions.eq("user", user));
			criteria.add(Restrictions.in("word.id_word", wordsId));
			List<UserWord> userWords = (List<UserWord>) criteria.list();
			return userWords;
		} else {
			return new ArrayList<UserWord>();
		}
	}

	@Override
	public UserWord findUserWordByUserAndWord(User user, Word word) {
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("user", user));
		criteria.add(Restrictions.eq("word", word));
		UserWord userWord = (UserWord) criteria.uniqueResult();
		return userWord;
	}

	@Override
	public void saveWordForUsers(Word word, List<User> users) {
		for (User user : users) {
			UserWord entity = new UserWord();
			entity.setUser(user);
			entity.setWord(word);
			entity.setRepetition_correct(0);
			entity.setRepetition_count(0);
			entity.setRepetition_row(0);
			entity.setNext_repetition(new LocalDateTime());
			persist(entity);
		}
	}

	@Override
	public void saveWordsForUser(User user, List<Word> words) {
		for (Word word : words) {
			UserWord entity = new UserWord();
			entity.setUser(user);
			entity.setWord(word);
			entity.setRepetition_correct(0);
			entity.setRepetition_count(0);
			entity.setRepetition_row(0);
			entity.setNext_repetition(new LocalDateTime().minusMinutes(1));
			persist(entity);
		}
	}

	@Override
	public void deleteUserWords(List<UserWord> userWords) {
		for (UserWord userWord : userWords) {
			delete(userWord);
		}
	}

}