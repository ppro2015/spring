package cz.uhk.ppro.dao;

import java.util.List;
import cz.uhk.ppro.model.Category;

/**
 * DAO class for {@link Category} class
 */
public interface CategoryDao {

	/**
	 * Returns {@link Category} by its id
	 * @param id
	 * @return {@link Category}
	 */
	Category findById(int id);

	/**
	 * Saves new category to database
	 * @param category instance of category to save
	 */
	void saveCategory(Category category);

	/**
	 * Deletes {@link Category} from database
	 * @param category instance of category to delete
	 */
	void deleteCategory(Category category);

	/**
	 * Finds all {@link Category}es
	 * @return {@link List} of all {@link Category}es
	 */
	List<Category> findAllCategories();

	/**
	 * Finds {@link Category} by title 
	 * @param title title of category
	 * @return {@link Category}
	 */
	Category findCategoryByTitle(String title);

	/**
	 * Finds {@link Category} by url 
	 * @param url url of category
	 * @return {@link Category}
	 */
	Category findCategoryByUrl(String url);

	/**
	 * Finds all {@link Category}es with no parent (they are potentially parent categories) 
	 * @return {@link List} of all {@link Category}es that have null parent
	 */
	List<Category> findAllNonParentCategories();

}