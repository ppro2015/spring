package cz.uhk.ppro.dao;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Repository;

import cz.uhk.ppro.common.CommonConstants;
import cz.uhk.ppro.model.User;

@Repository("userDao")
public class UserDaoImpl extends AbstractDao<Integer, User> implements UserDao {

	@Override
	public User findById(int id) {
		return getByKey(id);
	}

	@Override
	public User findUserByNickname(String nickname) {
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("nickname", nickname));
		return (User) criteria.uniqueResult();
	}

	@Override
	public void createUser(User user) {
		String pw_hash = BCrypt.hashpw(user.getPassword(), BCrypt.gensalt(CommonConstants.BCRYPT_STRENGTH));
		Query query = getSession().createSQLQuery("INSERT INTO `user` (`nickname`, `password`, `role`)"+
				"VALUES (:nickname, :pw_hash, :role);");

		query.setString("nickname", user.getNickname());
		query.setString("pw_hash", pw_hash);
		query.setString("role", user.getRole().name());

		query.executeUpdate();
	}

	@Override
	public void deleteUser(User user) {
		delete(user);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> findAllUsers() {
		Criteria criteria = createEntityCriteria();
		List<User> users = (List<User>) criteria.list();
		return users;
	}

	@Override
	public List<User> findAllUsersWithUserTests() {
		List<User> users = findAllUsers();
		for (User user : users) {
			Hibernate.initialize(user.getUserTests());
		}
		return users;
	}

	@Override
	public void updateUserDailyDone(User user) {
		User entity = findById(user.getId_user());
		if (entity != null) {

			int now = Calendar.getInstance().get(java.util.Calendar.DAY_OF_MONTH);
			Calendar cal = Calendar.getInstance();
			cal.setTime( entity.getLast_update_daily_done() );
			int old = cal.get(Calendar.DAY_OF_MONTH);

			if (old != now) {
				entity.setLast_update_daily_done(new Date());
				entity.setDaily_done(0);
			}
		}

		getSession().flush();
	}

}