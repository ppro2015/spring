package cz.uhk.ppro.dao;

import java.util.List;

import cz.uhk.ppro.model.User;
import cz.uhk.ppro.model.UserTest;

/**
 * DAO class for {@link User} class
 */
public interface UserDao {

	/**
	 * Returns {@link User} by its id
	 * @param id id of user to find 
	 * @return {@link User}
	 */
	User findById(int id);

	/**
	 * Finds {@link User} by nickname
	 * @param nickname nickname of user to find 
	 * @return {@link User}
	 */
	User findUserByNickname(String nickname);

	/**
	 * Creates/Saves new user to database
	 * @param user instance of user to create
	 */
	void createUser(User user);

	/**
	 * Deletes {@link User} from database
	 * @param user instance of user to delete
	 */
	void deleteUser(User user);

	/**
	 * Finds all {@link User}s
	 * @return {@link List} of all {@link User}s
	 */
	List<User> findAllUsers();

	/**
	 * Finds all {@link User}s with initialized {@link UserTest}s
	 * @return {@link List} of all {@link User}s
	 */
	List<User> findAllUsersWithUserTests();

	/**
	 * Updates {@link User}'s daily done count of points
	 * @param user {@link User}
	 */
	void updateUserDailyDone(User user);

}