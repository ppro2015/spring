package cz.uhk.ppro.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import cz.uhk.ppro.model.Category;

@Repository("categoryDao")
public class CategoryDaoImpl extends AbstractDao<Integer, Category> implements CategoryDao {

	@Override
	public Category findById(int id) {
		return getByKey(id);
	}

	@Override
	public void saveCategory(Category category) {
		persist(category);
	}

	@Override
	public void deleteCategory(Category category) {
		delete(category);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Category> findAllCategories() {
		Criteria criteria = createEntityCriteria().addOrder(Order.asc("id_category"));
		return (List<Category>) criteria.list();
	}

	@Override
	public Category findCategoryByTitle(String title) {
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("title", title));
		return (Category) criteria.uniqueResult();
	}

	@Override
	public Category findCategoryByUrl(String url) {
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("url", url));
		return (Category) criteria.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Category> findAllNonParentCategories() {
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.isNull("id_parent"));
		return (List<Category>) criteria.list();
	}

}