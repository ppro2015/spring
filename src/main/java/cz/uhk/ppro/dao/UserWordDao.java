package cz.uhk.ppro.dao;

import java.util.List;

import cz.uhk.ppro.model.Test;
import cz.uhk.ppro.model.User;
import cz.uhk.ppro.model.UserWord;
import cz.uhk.ppro.model.Word;

/**
 * DAO class for {@link UserWord} class
 */
public interface UserWordDao {

	/**
	 * Finds all {@link UserWord}s by {@link User}
	 * - meaning all {@link User}s' {@link UserWord}s
	 * @param user 
	 * @return {@link List} of {@link UserWord}
	 */
	List<UserWord> findAllUserWordsByUser(User user);

	/**
	 * Finds all {@link UserWord}s by {@link User} and {@link Test}
	 * - meaning all {@link User}s' {@link UserWord}s for given {@link Test}
	 * @param user 
	 * @param test 
	 * @return {@link List} of {@link UserWord}
	 */
	List<UserWord> findAllUserWordsForReview(User user, Test test);

	/**
	 * Finds all {@link UserWord}s by id_word
	 * - meaning all {@link Word}s' {@link UserWord}s
	 * @param id_word id of word
	 * @return {@link List} of {@link UserWord}
	 */
	List<UserWord> findAllUserWordsByWord(Word word);

	/**
	 * Finds all {@link UserWord}s by {@link User} and words
	 * - meaning all {@link UserWord}s for given {@link User} and list of {@link Word}s
	 * (used for deleting of test)
	 * @param user
	 * @param words {@link List} of {@link Word}s
	 * @return {@link List} of {@link UserWord}
	 */
	List<UserWord> findAllUserWordsByUserAndWords(User user, List<Word> words);

	/**
	 * Finds one {@link UserWord} by {@link User} and {@link Word}
	 * @param user
	 * @param word
	 * @return {@link UserWord}
	 */
	UserWord findUserWordByUserAndWord(User user, Word word);

	/**
	 * Creates {@link UserWord}s for given {@link Word} and for all {@link User}s that are assigned for test where this word is placed
	 * - used when creating new word
	 * @param word instance of {@link Word}
	 */
	void saveWordForUsers(Word word, List<User> users);

	/**
	 * Creates {@link UserWord}s for given {@link User} and {@link List} of {@link Word}s
	 * - used when adding user to test
	 * @param user instance of {@link User}
	 * @param words {@link List} of {@link Word}s
	 */
	void saveWordsForUser(User user, List<Word> words);

	/**
	 * Deletes given {@link List} of {@link UserWord}s
	 * @param userWords {@link List} of {@link UserWord}s
	 */
	void deleteUserWords(List<UserWord> userWords);

}