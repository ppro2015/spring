package cz.uhk.ppro.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.joda.time.LocalDateTime;
import org.springframework.stereotype.Repository;

import cz.uhk.ppro.model.Test;
import cz.uhk.ppro.model.User;
import cz.uhk.ppro.model.UserTest;

@Repository("userTestDao")
public class UserTestDaoImpl extends AbstractDao<Integer, UserTest> implements UserTestDao {

	@Override
	public UserTest findUserTestByUserAndTest(User user, Test test) {
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("user", user));
		criteria.add(Restrictions.eq("test", test));
		UserTest userTest = (UserTest) criteria.uniqueResult();
		return userTest;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UserTest> findAllUserTestByUser(User user) {
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("user", user));
		List<UserTest> userTests = (List<UserTest>) criteria.list();
		return userTests;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UserTest> findAllUserTestsByTest(Test test) {
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("test", test));
		List<UserTest> userTests = (List<UserTest>) criteria.list();
		return userTests;
	}

	@Override
	public void addUserToTest(User user, Test test) {
		UserTest userTest = new UserTest();
		userTest.setUser(user);
		userTest.setTest(test);
		userTest.setStart_date(new LocalDateTime());
		userTest.setPoints(0);
		persist(userTest);
	}

	@Override
	public void deleteUserTest(UserTest userTest) {
		delete(userTest);

	}

	@Override
	public void deleteUserTests(List<UserTest> userTests) {
		for (UserTest userTest : userTests) {
			delete(userTest);
		}
	}

}