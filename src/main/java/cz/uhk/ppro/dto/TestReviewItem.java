package cz.uhk.ppro.dto;

import java.io.Serializable;

import cz.uhk.ppro.model.UserWord;

public class TestReviewItem implements Serializable {

	private static final long serialVersionUID = 6954722362899563439L;

	private int id;
	private String key;
	private int repetition_count;
	private int repetition_correct;
	private int repetition_row;

	public TestReviewItem(UserWord userWord) {
		this.id = userWord.getWord().getId_word();
		this.key = userWord.getWord().getKey();
		this.repetition_count = userWord.getRepetition_count();
		this.repetition_correct = userWord.getRepetition_correct();
		this.repetition_row = userWord.getRepetition_row();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public int getRepetition_count() {
		return repetition_count;
	}

	public void setRepetition_count(int repetition_count) {
		this.repetition_count = repetition_count;
	}

	public int getRepetition_correct() {
		return repetition_correct;
	}

	public void setRepetition_correct(int repetition_correct) {
		this.repetition_correct = repetition_correct;
	}

	public int getRepetition_row() {
		return repetition_row;
	}

	public void setRepetition_row(int repetition_row) {
		this.repetition_row = repetition_row;
	}

}
