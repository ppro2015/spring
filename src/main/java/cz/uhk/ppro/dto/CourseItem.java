package cz.uhk.ppro.dto;

public class CourseItem {

	private String picture;
	private String title;
	private int id;
	private int countForReview;

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCountForReview() {
		return countForReview;
	}

	public void setCountForReview(int countForReview) {
		this.countForReview = countForReview;
	}

}
