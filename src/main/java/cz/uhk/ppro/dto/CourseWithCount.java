package cz.uhk.ppro.dto;

import cz.uhk.ppro.model.Category;
import cz.uhk.ppro.model.User;

/**
 * Usage on page with list of all courses
 */
public class CourseWithCount {

	private Integer id_test;
	private String title;
	private String picture;
	private User author;
	private Category category;
	private Long userCount;

	public CourseWithCount() {}

	public CourseWithCount(Object[] testObject) {
		this.id_test = (Integer) testObject[0];
		this.title = (String) testObject[1];
		this.picture = (String) testObject[2];
		this.author = (User) testObject[3];
		this.category = (Category) testObject[4];
		this.userCount = (Long) testObject[7];
	}

	public Integer getId_test() {
		return id_test;
	}

	public void setId_test(Integer id_test) {
		this.id_test = id_test;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public User getAuthor() {
		return author;
	}

	public void setAuthor(User author) {
		this.author = author;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public Long getUserCount() {
		return userCount;
	}

	public void setUserCount(Long userCount) {
		this.userCount = userCount;
	}

	@Override
	public String toString() {
		return "CourseWithCount [id_test=" + id_test + ", title=" + title + ", picture=" + picture + ", author="
				+ author + ", category=" + category + ", userCount=" + userCount + "]";
	}

}
