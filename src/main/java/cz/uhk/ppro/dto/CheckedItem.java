package cz.uhk.ppro.dto;

import java.io.Serializable;

public class CheckedItem implements Serializable {

	private static final long serialVersionUID = -7588469681501510958L;

	private int points;
	private String answer;
	private String correctAnswer;
	private boolean correct;

	private int repetition_count;
	private int repetition_correct;
	private int repetition_row;

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getCorrectAnswer() {
		return correctAnswer;
	}

	public void setCorrectAnswer(String correctAnswer) {
		this.correctAnswer = correctAnswer;
	}

	public boolean isCorrect() {
		return correct;
	}

	public void setCorrect(boolean correct) {
		this.correct = correct;
	}

	public int getRepetition_count() {
		return repetition_count;
	}

	public void setRepetition_count(int repetition_count) {
		this.repetition_count = repetition_count;
	}

	public int getRepetition_correct() {
		return repetition_correct;
	}

	public void setRepetition_correct(int repetition_correct) {
		this.repetition_correct = repetition_correct;
	}

	public int getRepetition_row() {
		return repetition_row;
	}

	public void setRepetition_row(int repetition_row) {
		this.repetition_row = repetition_row;
	}

	@Override
	public String toString() {
		return "CheckedItem [points=" + points + ", answer=" + answer + ", correctAnswer=" + correctAnswer
				+ ", correct=" + correct + ", repetition_count=" + repetition_count + ", repetition_correct="
				+ repetition_correct + ", repetition_row=" + repetition_row + "]";
	}

}
