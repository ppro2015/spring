package cz.uhk.ppro.common;

public class CommonConstants {
	/**
	 * Constant for password strength
	 */
	public static final int BCRYPT_STRENGTH = 11;

	/**
	 * Base value for calculating words repetition
	 */
	public static final int REPETITION_BASE_HOUR = 4;
}
