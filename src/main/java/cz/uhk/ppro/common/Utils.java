package cz.uhk.ppro.common;

import java.text.Normalizer;
import java.util.regex.Pattern;

public class Utils {

	/**
	 * Used for making URL from inserted Strings
	 * @param str
	 * @return URL
	 */
	public static String urlifyString(String str) {
		String norm = Normalizer.normalize(str, Normalizer.Form.NFD);
		Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
		return pattern.matcher(norm).replaceAll("").replace(" ", "-").toLowerCase();
	}
}
