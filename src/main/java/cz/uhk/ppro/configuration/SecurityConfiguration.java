package cz.uhk.ppro.configuration;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.web.filter.CharacterEncodingFilter;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
	DataSource dataSource;

	@Autowired
	CustomSuccessHandler customSuccessHandler;

	@Autowired
	public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception {

		auth.jdbcAuthentication().dataSource(dataSource)
			.passwordEncoder(passwordEncoder())
			.usersByUsernameQuery(
				"select nickname, password, enabled from user where nickname=?")
			.authoritiesByUsernameQuery(
				"select nickname, role from user where nickname=?");
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		PasswordEncoder encoder = new BCryptPasswordEncoder();
		return encoder;
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		//necessary for UTF-8 to work
		CharacterEncodingFilter filter = new CharacterEncodingFilter();
		filter.setEncoding("UTF-8");
		filter.setForceEncoding(true);
		http.addFilterBefore(filter, CsrfFilter.class);

		http.authorizeRequests()
			.antMatchers("/users/**").access("hasRole('ROLE_SUPERUSER')")
			.antMatchers("/tests/**").access("hasRole('ROLE_SUPERUSER')")
			.antMatchers("/test/**").access("hasRole('ROLE_SUPERUSER')")
			.antMatchers("/categories/**").access("hasAnyRole('ROLE_SUPERUSER','ROLE_ADMIN')")
			.antMatchers("/words/**").access("hasRole('ROLE_SUPERUSER')")
			.antMatchers("/home/**").access("hasAnyRole('ROLE_USER','ROLE_ADMIN')")
			.antMatchers("/courses/**").access("hasAnyRole('ROLE_USER','ROLE_ADMIN')")
			.antMatchers("/settings/**").access("hasAnyRole('ROLE_USER','ROLE_ADMIN')")
			.and()
				.formLogin().loginPage("/login").successHandler(customSuccessHandler).failureUrl("/login?error")
				.usernameParameter("nickname").passwordParameter("password")
			.and()
				.logout().logoutSuccessUrl("/login?logout")
			.and()
				.exceptionHandling().accessDeniedPage("/403");
	}

}
