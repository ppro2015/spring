package cz.uhk.ppro.configuration;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.stereotype.Component;

@Component
public class CustomSuccessHandler extends SimpleUrlAuthenticationSuccessHandler{

	private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

	/**
	 * Handles redirections after has logged in
	 */
	@Override
	protected void handle(HttpServletRequest request, 
		HttpServletResponse response, Authentication authentication) throws IOException {

		SavedRequest savedRequest = new HttpSessionRequestCache().getRequest(request, response);
		String targetUrl;
		if (savedRequest != null) {
			targetUrl = savedRequest.getRedirectUrl();
		} else {
			targetUrl = determineTargetUrl(authentication);
		}

		if (response.isCommitted()) {
			System.out.println("Can't redirect");
			return;
		}

		redirectStrategy.sendRedirect(request, response, targetUrl);
	}

	/**
	 * Return appropriate URL according to the logged user role
	 * @param authentication
	 * @return URL
	 */
	public String determineTargetUrl(Authentication authentication) {
		String url = "";

		Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();

		List<String> roles = new ArrayList<String>();

		for (GrantedAuthority a : authorities) {
			roles.add(a.getAuthority());
		}

		if (isSuperUser(roles)) {
			url = "/users/";
		} else if (isAdmin(roles)) {
			url = "/home/";
		} else if (isUser(roles)) {
			url = "/home/";
		} else {
			url="/403";
		}
		return url;
	}

	public void setRedirectStrategy(RedirectStrategy redirectStrategy) {
		this.redirectStrategy = redirectStrategy;
	}

	protected RedirectStrategy getRedirectStrategy() {
		return redirectStrategy;
	}

	private boolean isUser(List<String> roles) {
		return roles.contains("ROLE_USER");
	}

	private boolean isAdmin(List<String> roles) {
		return roles.contains("ROLE_ADMIN");
	}

	private boolean isSuperUser(List<String> roles) {
		return roles.contains("ROLE_SUPERUSER");
	}
}
