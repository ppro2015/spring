package cz.uhk.ppro.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import cz.uhk.ppro.dto.CheckedItem;
import cz.uhk.ppro.dto.TestReviewItem;
import cz.uhk.ppro.model.Test;
import cz.uhk.ppro.model.User;
import cz.uhk.ppro.model.UserWord;
import cz.uhk.ppro.model.Word;

@Controller
@RequestMapping("/courses/")
public class CourseReviewController extends AbstractController {
	private final String BASE_VIEW = "courses/";

	@RequestMapping(value = { "/review/{id_test}" }, method = RequestMethod.GET)
	public String courseDetail(@PathVariable int id_test, ModelMap model, RedirectAttributes redirectAttributes) {
		Test test = testService.findTestById(id_test);
		if (test == null) {
			redirectAttributes.addFlashAttribute("error", "Test was not found!");
			return "redirect:/courses/";
		} else {
			model.addAttribute("test", test);
			return BASE_VIEW + "review";
		}
	}

	@RequestMapping(value = "/review/{id_test}/get-words", method = RequestMethod.GET)
	@ResponseBody
	public Object loadWordsForReview(@PathVariable int id_test, HttpServletResponse response) throws Exception {
		Test test = testService.findTestById(id_test);
		if (test == null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Test was not found!");
		} else {
			User user = getCurrentUser();
			userService.updateUserDailyDone(user);
			List<UserWord> userWords = userWordService.findAllUserWordsForReview(user, test);
			List<TestReviewItem> testReviewItems = new ArrayList<>(userWords.size());
			for (UserWord userWord : userWords) {
				testReviewItems.add(new TestReviewItem(userWord));
			}
			Collections.shuffle(testReviewItems);
			return ResponseEntity.ok(testReviewItems);
		}
	}

	@RequestMapping(value = "/review/{id_test}/check-word", method = RequestMethod.GET)
	@ResponseBody
	public Object checkWordInReview(@PathVariable int id_test, @RequestParam String answer, @RequestParam int id_word) throws Exception {
		Test test = testService.findTestById(id_test);
		Word word = wordService.findWordById(id_word);
		if (test == null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Test was not found!");
		} else if (word == null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Word was not found!");
		} else {
			User user = getCurrentUser();
			List<UserWord> userWords = userWordService.findAllUserWordsForReview(user, test);
			UserWord reviewWord = userWordService.findUserWordByUserAndWord(user, word);
			if (userWords.contains(reviewWord)) {
				//check word
				CheckedItem checkedItem = userWordService.checkWord(reviewWord, answer);
				//add points in test for user
				int points = userTestService.addPoints(user, test, reviewWord);
				checkedItem.setPoints(points);
				//add points directly to user
				user.setPoints(user.getPoints() + points);
				user.setDaily_done(user.getDaily_done() + points);
				userService.updateUser(user);

				return ResponseEntity.ok(checkedItem);
			} else {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Word was not found in current test!");
			}
		}
	}
}
