package cz.uhk.ppro.controller;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletResponse;

import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import cz.uhk.ppro.model.Test;
import cz.uhk.ppro.model.User;
import cz.uhk.ppro.model.UserTest;

@Controller
@RequestMapping("/settings/")
public class SettingsController extends AbstractController {
	private final String BASE_VIEW = "settings/";

	@RequestMapping(value = { "/" }, method = RequestMethod.GET)
	public String basicSettings() {
		return "redirect:/" + BASE_VIEW + "basic/";
	}

	@RequestMapping(value = { "/basic/" }, method = RequestMethod.GET)
	public String settingsPage(ModelMap model) {
		User user = getCurrentUser();
		model.addAttribute("user", user);
		return BASE_VIEW + "basic";
	}

	@RequestMapping(value = { "/basic/daily-goal" }, method = RequestMethod.POST)
	public String updateUserDailyGoal(@Validated(User.ValidationDailyGoal.class) User user, BindingResult result,
			RedirectAttributes redirectAttributes) {
		if (result.hasErrors()) {
			return BASE_VIEW + "basic";
		}

		User currentUser = getCurrentUser();
		currentUser.setDaily_goal(user.getDaily_goal());
		userService.updateUser(currentUser);

		redirectAttributes.addFlashAttribute("success", "Your daily goal was successfully updated.");
		return "redirect:/" + BASE_VIEW + "basic/";
	}

	@RequestMapping(value = { "/basic/password" }, method = RequestMethod.POST)
	public String updateUserPassword(@Validated(User.ValidationPassword.class) User user, BindingResult result,
			RedirectAttributes redirectAttributes) {

		Locale locale = LocaleContextHolder.getLocale();

		if(!user.getPassword().equals(user.getConfirmPassword())) {
			result.addError(new FieldError("user", "password",
				messageSource.getMessage("non.equal.passwords", new String[]{}, locale))
			);
		}

		if (result.hasErrors()) {
			return BASE_VIEW + "basic";
		}

		userService.updateUserPassword(user);

		redirectAttributes.addFlashAttribute("success", "Your password was successfully updated.");
		return "redirect:/settings/basic/";
	}

	@RequestMapping(value = { "/tests/" }, method = RequestMethod.GET)
	public String testsSettings(ModelMap model, RedirectAttributes redirectAttributes, HttpServletResponse response) {
		User user = getCurrentUser();
		List<UserTest> userTests = userTestService.findAllUserTestByUser(user);
		model.addAttribute("userTests", userTests);
		return BASE_VIEW + "tests";
	}

	@RequestMapping(value = "/tests/ajax-delete/{id_test}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<String> reallyDeleteTestAjax(@PathVariable int id_test) {
		Test test = testService.findTestById(id_test);
		User user = getCurrentUser();
		UserTest userTest = userTestService.findUserTestByUserAndTest(user, test);
		if (test == null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Test was not found!");
		} else if (user == null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("User was not found!");
		} else if (userTest == null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("User is not assigned to this test!");
		} else {
			deleteUserFromTest(user, test, userTest);
			return ResponseEntity.ok("");
		}
	}
}
