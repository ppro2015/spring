package cz.uhk.ppro.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import cz.uhk.ppro.model.Test;
import cz.uhk.ppro.model.User;
import cz.uhk.ppro.model.UserTest;

@Controller
@RequestMapping("/test/")
public class TestUsersController extends AbstractController {

	private final String BASE_VIEW = "test-users/";
	private final String REDIRECT_URL = "tests/";

	@RequestMapping(value = "/{id_test}/users", method = RequestMethod.GET)
	public String showUsersForTest(@PathVariable int id_test, ModelMap model, RedirectAttributes redirectAttributes) {
		Test test = testService.findTestById(id_test);
		if (test == null) {
			redirectAttributes.addFlashAttribute("error", "Test was not found!");
			return "redirect:/" + REDIRECT_URL;
		} else {
			prepareShowUsers(model, test);
			return BASE_VIEW + "users";
		}
	}

	@RequestMapping(value = "/{id_test}/users", method = RequestMethod.POST)
	public String addUserToTest(@PathVariable int id_test, User u, BindingResult result,
			ModelMap model, RedirectAttributes redirectAttributes) {

		Test test = testService.findTestById(id_test);
		if (test == null) {
			redirectAttributes.addFlashAttribute("error", "Test was not found!");
			return "redirect:/" + REDIRECT_URL;
		} else {
			int id_user = u.getId_user();
			User user = userService.findUserById(id_user);
			if (user == null) {
				FieldError testAuthorError = new FieldError("user", "id_user", "User was not found.");
				result.addError(testAuthorError);
				// FIXME testAuthorError se neobjevuje
				prepareShowUsers(model, test);
				return BASE_VIEW + "users";
			}

			addUserToTest(user, test);

			redirectAttributes.addFlashAttribute("success", "User '" + user.getNickname() + "' was successfully added.");
			return "redirect:/test/" + id_test + "/users";
		}
	}

	private void prepareShowUsers(ModelMap model, Test test) {
		//aktu�ln� u�ivatel�
		List<UserTest> usersInTest = userTestService.findAllUserTestsByTest(test);
		model.addAttribute("test_users", usersInTest);
		//kv�li p�id�n� nov�ho
		model.addAttribute("user", new User());
		model.addAttribute("newUsers", getUsersMapForAdding(usersInTest));
		//pro zobrazen� informac� o testu
		model.addAttribute("test", test);
	}

	@RequestMapping(value = "/{id_test}/ajax-add-user/{id_user}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<String> addUserToTestAjax(@PathVariable int id_test, @PathVariable int id_user) {
		Test test = testService.findTestById(id_test);
		User user = userService.findUserById(id_user);
		if (test == null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Test was not found!");
		} else if (user == null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("User was not found!");
		} else {
			addUserToTest(user, test);
			return ResponseEntity.ok("");
		}
	}

	@RequestMapping(value = { "/{id_test}/delete-user/{id_user}" }, method = RequestMethod.GET)
	public String deleteUserFromTest(@PathVariable int id_test, @PathVariable int id_user,
			ModelMap model, RedirectAttributes redirectAttributes) {
		Test test = testService.findTestById(id_test);
		User user = userService.findUserById(id_user);
		UserTest userTest = userTestService.findUserTestByUserAndTest(user, test);
		if (test == null) {
			redirectAttributes.addFlashAttribute("error", "Test was not found!");
			return "redirect:/" + REDIRECT_URL;
		} else if (user == null) {
			redirectAttributes.addFlashAttribute("error", "User was not found!");
			return "redirect:/test/" + id_test + "/users";
		} else if (userTest == null) {
			redirectAttributes.addFlashAttribute("error", "User is not assigned to this test!");
			return "redirect:/test/" + id_test + "/users";
		} else {
			model.addAttribute("test", test);
			model.addAttribute("user", user);
			return BASE_VIEW + "delete-user";
		}
	}

	@RequestMapping(value = { "/{id_test}/delete-user/{id_user}" }, method = RequestMethod.POST)
	public String reallyDeleteUserFromTest(@PathVariable int id_test, @PathVariable int id_user,
			ModelMap model, RedirectAttributes redirectAttributes) {
		Test test = testService.findTestById(id_test);
		User user= userService.findUserById(id_user);
		UserTest userTest = userTestService.findUserTestByUserAndTest(user, test);
		if (test == null) {
			redirectAttributes.addFlashAttribute("error", "Test was not found!");
			return "redirect:/" + REDIRECT_URL;
		} else if (user == null) {
			redirectAttributes.addFlashAttribute("error", "User was not found!");
		} else if (userTest == null) {
			redirectAttributes.addFlashAttribute("error", "User is not assigned to this test!");
		} else {
			deleteUserFromTest(user, test, userTest);
			redirectAttributes.addFlashAttribute("success", "User '" + user.getNickname() + "' was successfully deleted from this test.");
		}
		return "redirect:/test/" + id_test + "/users";
	}

	@RequestMapping(value = "/{id_test}/ajax-delete-user/{id_user}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<String> reallyDeleteUserFromTestAjax(@PathVariable int id_test, @PathVariable int id_user) {
		Test test = testService.findTestById(id_test);
		User user = userService.findUserById(id_user);
		UserTest userTest = userTestService.findUserTestByUserAndTest(user, test);
		if (test == null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Test was not found!");
		} else if (user == null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("User was not found!");
		} else if (userTest == null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("User is not assigned to this test!");
		} else {
			deleteUserFromTest(user, test, userTest);
			return ResponseEntity.ok("");
		}
	}

	/**
	 * Map of users that are not currently in this test
	 * @param usersInTest List of users currently added in test
	 * @return Map containing users
	 */
	private Map<Integer, String> getUsersMapForAdding(List<UserTest> usersInTest) {
		//this method is based on fact that nickname is unique property

		//existing users
		List<String> existingUsersNicks = new ArrayList<>(usersInTest.size());
		for (UserTest userTest : usersInTest) {
			existingUsersNicks.add(userTest.getUser().getNickname());
		}

		//all users
		List<User> users = userService.findAllUsers();
		List<String> usersNicks = new ArrayList<>(users.size());
		for (User user : users) {
			usersNicks.add(user.getNickname());
		}

		Map<Integer, String> usersMap = new HashMap<Integer, String>();
		for (int i = 0; i < usersNicks.size(); i++) {
			if (!existingUsersNicks.contains(usersNicks.get(i))) {
				usersMap.put(users.get(i).getId_user(), users.get(i).getNickname());
			}
		}
		return usersMap;
	}

}