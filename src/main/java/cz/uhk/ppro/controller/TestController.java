package cz.uhk.ppro.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import cz.uhk.ppro.model.Category;
import cz.uhk.ppro.model.Test;
import cz.uhk.ppro.model.User;

@Controller
@RequestMapping("/tests/")
public class TestController extends AbstractController {
	private final String BASE_VIEW = "tests/";

	@RequestMapping(value = { "/" }, method = RequestMethod.GET)
	public String listTests(ModelMap model) {
		List<Test> tests = testService.findAllTests();
		model.addAttribute("tests", tests);
		return BASE_VIEW + "all";
	}

	@RequestMapping(value = { "/new" }, method = RequestMethod.GET)
	public String newTest(ModelMap model) {
		model.addAttribute("test", new Test());
		model.addAttribute("users", getUsersMap());
		model.addAttribute("categories", getCategoriesMap());
		return BASE_VIEW + "new";
	}

	@RequestMapping(value = { "/new" }, method = RequestMethod.POST)
	public String saveTest(@Valid Test test, BindingResult result, ModelMap model, RedirectAttributes redirectAttributes) {

		User user = userService.findUserById(test.getAuthor().getId_user());
		if (user == null) {
			FieldError testAuthorError = new FieldError("test", "author.id_user", "Author was not found.");
			result.addError(testAuthorError);
		}

		Category category = categoryService.findCategoryById(test.getCategory().getId_category());
		if (category == null) {
			FieldError testAuthorError = new FieldError("test", "category.id_category", "Category was not found.");
			result.addError(testAuthorError);
		}

		if (result.hasErrors()) {
			model.addAttribute("users", getUsersMap());
			model.addAttribute("categories", getCategoriesMap());
			return BASE_VIEW + "new";
		}

		testService.saveTest(test);

		redirectAttributes.addFlashAttribute("success", "Test '" + test.getTitle() + "' was successfully created.");
		return "redirect:/" + BASE_VIEW;
	}

	@RequestMapping(value = { "/edit/{id_test}" }, method = RequestMethod.GET)
	public String editTest(@PathVariable int id_test, ModelMap model, RedirectAttributes redirectAttributes) {
		Test test = testService.findTestById(id_test);
		if (test == null) {
			redirectAttributes.addFlashAttribute("error", "Test was not found!");
			return "redirect:/" + BASE_VIEW;
		} else {
			model.addAttribute("test", test);
			model.addAttribute("users", getUsersMap());
			model.addAttribute("categories", getCategoriesMap());
			return BASE_VIEW + "edit";
		}
	}

	@RequestMapping(value = { "/edit/{id_test}" }, method = RequestMethod.POST)
	public String updateTest(@PathVariable int id_test, @Valid Test test, BindingResult result,
			ModelMap model, RedirectAttributes redirectAttributes) {

		if (testService.findTestById(id_test) == null) {
			redirectAttributes.addFlashAttribute("error", "Test was not found!");
			return "redirect:/" + BASE_VIEW;
		} else {
			User user = userService.findUserById(test.getAuthor().getId_user());
			if (user == null) {
				FieldError testAuthorError = new FieldError("test", "author.id_user", "Author was not found.");
				result.addError(testAuthorError);
			}

			Category category = categoryService.findCategoryById(test.getCategory().getId_category());
			if (category == null) {
				FieldError testAuthorError = new FieldError("test", "category.id_category", "Category was not found.");
				result.addError(testAuthorError);
			}

			if (result.hasErrors()) {
				model.addAttribute("users", getUsersMap());
				model.addAttribute("categories", getCategoriesMap());
				return BASE_VIEW + "edit";
			}

			testService.updateTest(test);

			redirectAttributes.addFlashAttribute("success", "Test '" + test.getTitle() + "' was successfully updated.");
			return "redirect:/" + BASE_VIEW;
		}
	}

	@RequestMapping(value = { "/delete/{id_test}" }, method = RequestMethod.GET)
	public String deleteTest(@PathVariable int id_test, ModelMap model, RedirectAttributes redirectAttributes) {
		Test test = testService.findTestById(id_test);
		if (test == null) {
			redirectAttributes.addFlashAttribute("error", "Test was not found!");
			return "redirect:/" + BASE_VIEW;
		} else {
			model.addAttribute("test", test);
			return BASE_VIEW + "delete";
		}
	}

	@RequestMapping(value = { "/delete/{id_test}" }, method = RequestMethod.POST)
	public String reallyDeleteTest(@PathVariable int id_test, ModelMap model, RedirectAttributes redirectAttributes) {
		Test test = testService.findTestById(id_test);
		if (test == null) {
			redirectAttributes.addFlashAttribute("error", "Test was not found!");
		} else {
			deleteTest(test);
			redirectAttributes.addFlashAttribute("success", "Test '" + test.getTitle() + "' was successfully deleted.");
		}
		return "redirect:/" + BASE_VIEW;
	}

	@RequestMapping(value = "/ajax-delete/{id_test}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<String> reallyDeleteTestAjax(@PathVariable int id_test) {
		Test test = testService.findTestById(id_test);
		if (test == null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Test was not found!");
		} else {
			deleteTest(test);
			return ResponseEntity.ok("");
		}
	}

	private Map<Integer, String> getUsersMap() {
		List<User> users = userService.findAllUsers();
		Map<Integer, String> usersMap = new HashMap<Integer, String>();
		for (int i = 0; i < users.size(); i++) {
			usersMap.put(users.get(i).getId_user(), users.get(i).getNickname());
		}
		return usersMap;
	}

}