package cz.uhk.ppro.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import cz.uhk.ppro.model.Word;

@Controller
@RequestMapping("/words/")
public class WordsController extends AbstractController {
	private final String BASE_VIEW = "words/";

	@RequestMapping(value = { "/" }, method = RequestMethod.GET)
	public String showAllWords(ModelMap model) {
		List<Word> words = wordService.findAllWords();
		model.addAttribute("words", words);
		return BASE_VIEW + "allwords";
	}

	@RequestMapping(value = { "/delete/{id_word}" }, method = RequestMethod.GET)
	public String deleteWord(@PathVariable int id_word, ModelMap model, RedirectAttributes redirectAttributes) {
		Word word = wordService.findWordById(id_word);
		if (word == null) {
			redirectAttributes.addFlashAttribute("error", "Word was not found!");
			return "redirect:/words/";
		} else {
			model.addAttribute("word", word);
			return BASE_VIEW + "delete";
		}
	}

	@RequestMapping(value = { "/delete/{id_word}" }, method = RequestMethod.POST)
	public String reallyDeleteWord(@PathVariable int id_word, RedirectAttributes redirectAttributes) {
		Word word = wordService.findWordById(id_word);
		if (word == null) {
			redirectAttributes.addFlashAttribute("error", "Word was not found!");
			return "redirect:/words/";
		} else {
			deleteWord(word);
			redirectAttributes.addFlashAttribute("success", "Word '" + word.getKey() + "' was successfully deleted.");
			return "redirect:/test/" + word.getTest().getId_test() + "/words";
		}
	}

	@RequestMapping(value = "/ajax-delete/{id_word}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<String> reallyDeleteWordAjax(@PathVariable int id_word) {
		Word word = wordService.findWordById(id_word);
		if (word == null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Word was not found!");
		} else {
			deleteWord(word);
			return ResponseEntity.ok("");
		}
	}
}