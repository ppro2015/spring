package cz.uhk.ppro.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import cz.uhk.ppro.model.Category;
import cz.uhk.ppro.model.Test;
import cz.uhk.ppro.model.User;
import cz.uhk.ppro.model.UserTest;
import cz.uhk.ppro.model.UserWord;
import cz.uhk.ppro.model.Word;
import cz.uhk.ppro.service.CategoryService;
import cz.uhk.ppro.service.TestService;
import cz.uhk.ppro.service.UserService;
import cz.uhk.ppro.service.UserTestService;
import cz.uhk.ppro.service.UserWordService;
import cz.uhk.ppro.service.WordService;

public abstract class AbstractController {

	@Autowired
	TestService testService;
	@Autowired
	UserService userService;
	@Autowired
	WordService wordService;
	@Autowired
	CategoryService categoryService;
	@Autowired
	UserTestService userTestService;
	@Autowired
	UserWordService userWordService;

	@Autowired
	MessageSource messageSource;

	User getCurrentUser() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		User user = userService.findUserByNickname(name);
		return user;
	}

	Locale getlocale() {
		return LocaleContextHolder.getLocale();
	}

	/**
	 * Adds {@link User} to {@link Test} and test's words to {@link UserWord}
	 * @param user user to add to test
	 * @param test test to which to add the user
	 */
	void addUserToTest(User user, Test test) {
		userTestService.addUserToTest(user, test);

		List<Word> words = wordService.findAllWordsInTest(test);
		userWordService.saveWordsForUser(user, words);
	}

	/**
	 * Deletes {@link User} from {@link Test} and his words from {@link UserWord}
	 * @param user user to delete from test
	 * @param test test from which to delete the user
	 * @param userTest 
	 */
	void deleteUserFromTest(User user, Test test, UserTest userTest) {
		List<Word> words = wordService.findAllWordsInTest(test);
		List<UserWord> userWords = userWordService.findAllUserWordsByUserAndWords(user, words);
		userWordService.deleteUserWords(userWords);

		userTestService.deleteUserTest(userTest);
	}

	Map<Integer, String> getCategoriesMap() {
		List<Category> categories = categoryService.findAllCategories();
		Map<Integer, String> categoryMap = new HashMap<Integer, String>();
		for (int i = 0; i < categories.size(); i++) {
			categoryMap.put(categories.get(i).getId_category(), categories.get(i).getTitle());
		}
		return categoryMap;
	}

	void saveWord(Word word, Test test) {
		//save word
		word.setTest(test);
		wordService.saveWord(word);
		//create userwords
		List<UserTest> userTests = userTestService.findAllUserTestsByTest(test);
		List<User> users = new ArrayList<>(userTests.size());
		for (UserTest userTest : userTests) {
			users.add(userTest.getUser());
		}
		userWordService.saveWordForUsers(word, users);
	}

	void deleteTest(Test test) {
		List<Word> words = wordService.findAllWordsInTest(test);
		//delete userwords for test
		List<UserWord> userWords = new ArrayList<>();
		for (Word word : words) {
			userWords.addAll(userWordService.findAllUserWordsByWord(word));
		}
		userWordService.deleteUserWords(userWords);
		//delete words
		for (Word word : words) {
			wordService.deleteWord(word);
		}
		//delete usertests
		List<UserTest> userTests = userTestService.findAllUserTestsByTest(test);
		userTestService.deleteUserTests(userTests);
		//delete test
		test.setAuthor(null);
		test.setCategory(null);
		testService.deleteTest(test);
	}

	void deleteWord(Word word) {
		List<UserWord> userWords = userWordService.findAllUserWordsByWord(word);
		userWordService.deleteUserWords(userWords);
		wordService.deleteWord(word);
	}
}
