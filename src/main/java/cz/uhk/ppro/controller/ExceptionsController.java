package cz.uhk.ppro.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/")
public class ExceptionsController extends AbstractController {

	@RequestMapping(value = "/403", method = RequestMethod.GET)
	public String accesssDeniedPage() {
		return "403";
	}

}