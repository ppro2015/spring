package cz.uhk.ppro.controller;

import java.util.List;
import java.util.Locale;

import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import cz.uhk.ppro.model.User;
import cz.uhk.ppro.model.User.Role;
import cz.uhk.ppro.model.UserTest;
import cz.uhk.ppro.model.UserWord;

@Controller
@RequestMapping("/users/")
public class UserController extends AbstractController {
	private final String BASE_VIEW = "users/";

	@RequestMapping(value = { "/" }, method = RequestMethod.GET)
	public String listUsers(ModelMap model) {
		List<User> users = userService.findAllUsersWithUserTests();
		model.addAttribute("users", users);
		return BASE_VIEW + "all";
	}

	@RequestMapping(value = { "/new" }, method = RequestMethod.GET)
	public String newUser(ModelMap model) {
		User user = new User();
		model.addAttribute("user", user);
		model.addAttribute("role", Role.values());
		return BASE_VIEW + "new";
	}

	@RequestMapping(value = { "/new" }, method = RequestMethod.POST)
	public String saveUser(@Validated(User.ValidationCreateUser.class) User user,
			BindingResult result, RedirectAttributes redirectAttributes) {

		if(!user.getPassword().equals(user.getConfirmPassword())) {
			result.addError(new FieldError("user", "password",
				messageSource.getMessage("non.equal.passwords", new String[]{}, getlocale()))
			);
		}

		if (!userService.isUserNicknameUnique(user.getId_user(), user.getNickname())) {
			result.addError(new FieldError("user", "nickname",
				messageSource.getMessage("non.unique.nickname", new String[]{user.getNickname()}, getlocale()))
			);
		}

		if (result.hasErrors()) {
			return BASE_VIEW + "new";
		}

		userService.saveUser(user);

		redirectAttributes.addFlashAttribute("success", "User '" + user.getNickname() + "' was registered successfully.");
		return "redirect:/" + BASE_VIEW;
	}

	@RequestMapping(value = { "/edit/{id_user}" }, method = RequestMethod.GET)
	public String editUser(@PathVariable int id_user, ModelMap model, RedirectAttributes redirectAttributes) {
		User user = userService.findUserById(id_user);
		if (user == null) {
			redirectAttributes.addFlashAttribute("error", "User was not found!");
			return "redirect:/" + BASE_VIEW;
		} else {
			model.addAttribute("user", user);
			return BASE_VIEW + "edit";
		}
	}

	@RequestMapping(value = { "/edit/{id_user}" }, method = RequestMethod.POST)
	public String updateUser(@PathVariable int id_user, @Validated(User.ValidationUpdateUser.class) User user,
			BindingResult result, RedirectAttributes redirectAttributes) {

		Locale locale = LocaleContextHolder.getLocale();

		if (userService.findUserById(id_user) == null) {
			redirectAttributes.addFlashAttribute("error", "User was not found!");
			return "redirect:/" + BASE_VIEW;
		} else {
			if (!userService.isUserNicknameUnique(user.getId_user(), user.getNickname())) {
				result.addError(new FieldError("user", "nickname",
					messageSource.getMessage("non.unique.nickname", new String[]{user.getNickname()}, locale)) 
				);
			}

			if (result.hasErrors()) {
				return BASE_VIEW + "edit";
			}

			userService.updateUser(user);

			redirectAttributes.addFlashAttribute("success", "User '" + user.getNickname() + "' was successfully updated.");
			return "redirect:/" + BASE_VIEW;
		}
	}

	@RequestMapping(value = { "/edit-password/{id_user}" }, method = RequestMethod.GET)
	public String editUserPassword(@PathVariable int id_user, ModelMap model, RedirectAttributes redirectAttributes) {
		User user = userService.findUserById(id_user);
		if (user == null) {
			redirectAttributes.addFlashAttribute("error", "User was not found!");
			return "redirect:/" + BASE_VIEW;
		} else {
			model.addAttribute("user", user);
			return BASE_VIEW + "edit-password";
		}
	}

	@RequestMapping(value = { "/edit-password/{id_user}" }, method = RequestMethod.POST)
	public String updateUserPassword(@PathVariable int id_user, @Validated(User.ValidationPassword.class) User user, BindingResult result, RedirectAttributes redirectAttributes) {

		Locale locale = LocaleContextHolder.getLocale();

		if (userService.findUserById(id_user) == null) {
			redirectAttributes.addFlashAttribute("error", "User was not found!");
			return "redirect:/" + BASE_VIEW;
		} else {
			if(!user.getPassword().equals(user.getConfirmPassword())) {
				result.addError(new FieldError("user", "password",
					messageSource.getMessage("non.equal.passwords", new String[]{}, locale))
				);
			}

			if (result.hasErrors()) {
				return BASE_VIEW + "edit-password";
			}

			userService.updateUserPassword(user);

			redirectAttributes.addFlashAttribute("success", "User '" + user.getNickname() + "' was successfully updated.");
			return "redirect:/" + BASE_VIEW;
		}
	}

	@RequestMapping(value = { "/delete/{id_user}" }, method = RequestMethod.GET)
	public String deleteUser(@PathVariable int id_user, ModelMap model, RedirectAttributes redirectAttributes) {
		User user = userService.findUserById(id_user);
		if (user == null) {
			redirectAttributes.addFlashAttribute("error", "User was not found!");
			return "redirect:/" + BASE_VIEW;
		} else {
			model.addAttribute("user", user);
			return BASE_VIEW + "delete";
		}
	}

	@RequestMapping(value = { "/delete/{id_user}" }, method = RequestMethod.POST)
	public String reallyDeleteUser(@PathVariable int id_user, ModelMap model, RedirectAttributes redirectAttributes) {
		User user = userService.findUserById(id_user);
		if (user == null) {
			redirectAttributes.addFlashAttribute("error", "User was not found!");
		} else {
			deleteUser(user);
			redirectAttributes.addFlashAttribute("success", "User '" + user.getNickname() + "' was successfully deleted.");
		}
		return "redirect:/" + BASE_VIEW;
	}

	@RequestMapping(value = "/ajax-delete/{id_user}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<String> reallyDeleteUserAjax(@PathVariable int id_user) {
		User user = userService.findUserById(id_user);
		if (user == null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("User was not found!");
		} else {
			deleteUser(user);
			return ResponseEntity.ok("");
		}
	}

	private void deleteUser(User user) {
		//delete his user_words
		List<UserWord> userWords = userWordService.findAllUserWordsByUser(user);
		userWordService.deleteUserWords(userWords);
		//delete his user_tests
		List<UserTest> userTests = userTestService.findAllUserTestByUser(user);
		userTestService.deleteUserTests(userTests);
		//then delete him
		userService.deleteUser(user);
	}

	@RequestMapping(value = { "/words/{id_user}" }, method = RequestMethod.GET)
	public String listOfUserWords(@PathVariable int id_user, ModelMap model, RedirectAttributes redirectAttributes) {
		User user = userService.findUserById(id_user);
		if (user == null) {
			redirectAttributes.addFlashAttribute("error", "User was not found!");
			return "redirect:/" + BASE_VIEW;
		} else {
			List<UserWord> userWords = userWordService.findAllUserWordsByUser(user);
			model.addAttribute("userWords", userWords);
			model.addAttribute("user", user);
			return BASE_VIEW + "userwords";
		}
	}

}