package cz.uhk.ppro.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import cz.uhk.ppro.model.Category;
import cz.uhk.ppro.model.Test;
import cz.uhk.ppro.model.User;
import cz.uhk.ppro.model.Word;

@Controller
@RequestMapping("/courses/")
public class CourseCreationController extends AbstractController {
	private final String BASE_VIEW = "courses/";

	@RequestMapping(value = { "/create" }, method = RequestMethod.GET)
	public String createCourse(ModelMap model) {
		model.addAttribute("test", new Test());
		model.addAttribute("categories", getCategoriesMap());
		return BASE_VIEW + "create";
	}

	@RequestMapping(value = { "/create" }, method = RequestMethod.POST)
	public String saveNewCourse(@Valid Test test, BindingResult result, ModelMap model, RedirectAttributes redirectAttributes) {
		User user = getCurrentUser();
		test.setAuthor(user);
		Category category = categoryService.findCategoryById(test.getCategory().getId_category());
		if (category == null) {
			FieldError categoryError = new FieldError("test", "category.id_category", "Category was not found.");
			result.addError(categoryError);
		}

		if (result.hasErrors()) {
			model.addAttribute("categories", getCategoriesMap());
			return BASE_VIEW + "create";
		}

		testService.saveTest(test);

		redirectAttributes.addFlashAttribute("success", "Test '" + test.getTitle() + "' was successfully created.");
		return "redirect:/" + BASE_VIEW + "edit/" + test.getId_test();
	}

	@RequestMapping(value = { "/edit/{id_test}" }, method = RequestMethod.GET)
	public String editWholeTest(@PathVariable int id_test, ModelMap model, RedirectAttributes redirectAttributes) {
		Test test = testService.findTestById(id_test);
		User user = getCurrentUser();
		if (test == null) {
			redirectAttributes.addFlashAttribute("error", "Course was not found!");
			return "redirect:/" + BASE_VIEW;
		} else if (user.getId_user() != test.getAuthor().getId_user()) {
			redirectAttributes.addFlashAttribute("error", "You cannot edit this course!");
			return "redirect:/" + BASE_VIEW;
		} else {
			model.addAttribute("test", test);
			model.addAttribute("categories", getCategoriesMap());
			model.addAttribute("word", new Word());
			List<Word> words = wordService.findAllWordsInTest(test);
			model.addAttribute("words", words);
			return BASE_VIEW + "edit";
		}
	}

	@RequestMapping(value = { "/edit/{id_test}/test" }, method = RequestMethod.POST)
	public String updateTest(@PathVariable int id_test, @Valid Test test, BindingResult result,
			ModelMap model, RedirectAttributes redirectAttributes) {
		Test testEntity = testService.findTestById(id_test);
		User user = getCurrentUser();
		if (testEntity == null) {
			redirectAttributes.addFlashAttribute("error", "Course was not found!");
			return "redirect:/" + BASE_VIEW;
		} else if (user.getId_user() != testEntity.getAuthor().getId_user()) {
			redirectAttributes.addFlashAttribute("error", "You cannot edit this course!");
			return "redirect:/" + BASE_VIEW;
		} else {

			Category category = categoryService.findCategoryById(test.getCategory().getId_category());
			if (category == null) {
				FieldError testAuthorError = new FieldError("test", "category.id_category", "Category was not found.");
				result.addError(testAuthorError);
			}

			if (result.hasErrors()) {
				model.addAttribute("word", new Word());
				model.addAttribute("categories", getCategoriesMap());
				List<Word> words = wordService.findAllWordsInTest(test);
				model.addAttribute("words", words);

				//aby se zobrazovalo spr�vn� jm�no, kdy� do�lo k chyb� p�i validaci
				//zobrazovalo by se jm�no editovan�ho, kter� je�t� nen� ulo�en�
				model.addAttribute("testEntity", testEntity);
				return BASE_VIEW + "edit";
			}
			test.setAuthor(user);
			testService.updateTest(test);
			redirectAttributes.addFlashAttribute("success", "Test was successfully updated.");

			return "redirect:/" + BASE_VIEW + "edit/" + test.getId_test();
		}
	}

	@RequestMapping(value = { "/edit/{id_test}/new-word" }, method = RequestMethod.POST)
	public String saveWord(@PathVariable int id_test, @Valid Word word, BindingResult result,
			ModelMap model, RedirectAttributes redirectAttributes) {

		Test test = testService.findTestById(id_test);
		User user = getCurrentUser();
		if (test == null) {
			redirectAttributes.addFlashAttribute("error", "Course was not found!");
			return "redirect:/" + BASE_VIEW;
		} else if (user.getId_user() != test.getAuthor().getId_user()) {
			redirectAttributes.addFlashAttribute("error", "You cannot add words to this course!");
			return "redirect:/" + BASE_VIEW;
		} else {
			if (result.hasErrors()) {
				model.addAttribute("test", test);
				model.addAttribute("categories", getCategoriesMap());
				List<Word> words = wordService.findAllWordsInTest(test);
				model.addAttribute("words", words);
				return BASE_VIEW + "edit";
			}

			saveWord(word, test);
			redirectAttributes.addFlashAttribute("success", "Word '" + word.getAnswer() + "' was successfully created.");
			return "redirect:/" + BASE_VIEW + "edit/" + test.getId_test();
		}
	}

	@RequestMapping(value = "/edit/{id_test}/ajax-delete", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<String> reallyDeleteTestAjax(@PathVariable int id_test) {
		Test test = testService.findTestById(id_test);
		User user = getCurrentUser();
		if (test == null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Test was not found!");
		} else if (user.getId_user() != test.getAuthor().getId_user()) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("You cannot delete this course!");
		} else {
			deleteTest(test);
			return ResponseEntity.ok("");
		}
	}

	@RequestMapping(value = "/edit/{id_test}/ajax-delete-word/{id_word}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<String> reallyDeleteWordAjax(@PathVariable int id_test, @PathVariable int id_word) {
		Word word = wordService.findWordById(id_word);
		Test test = testService.findTestById(id_test);
		User user = getCurrentUser();
		if (word == null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Word was not found!");
		} else if (test == null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Test was not found!");
		} else if (user.getId_user() != test.getAuthor().getId_user()) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("You cannot edit this course!");
		} else if (!wordService.findAllWordsInTest(test).contains(word)) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("You cannot edit this course!");
		} else {
			deleteWord(word);
			return ResponseEntity.ok("");
		}
	}

	@RequestMapping(value = "/edit/{id_test}/ajax-edit-word/{id_word}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<String> editWordAjax(@PathVariable int id_test, @PathVariable int id_word,
			@RequestParam("key") String key, @RequestParam("answer") String answer) {
		Word word = wordService.findWordById(id_word);
		Test test = testService.findTestById(id_test);
		User user = getCurrentUser();
		if (word == null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Word was not found!");
		} else if (test == null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Test was not found!");
		} else if (user.getId_user() != test.getAuthor().getId_user()) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("You cannot edit this course!");
		} else if (!wordService.findAllWordsInTest(test).contains(word)) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("You cannot edit this course!");
		} else {
			word.setKey(key);
			word.setAnswer(answer);
			wordService.updateWord(word);
			return ResponseEntity.ok("");
		}
	}
}
