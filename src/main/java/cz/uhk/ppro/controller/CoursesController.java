package cz.uhk.ppro.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.LocalDateTime;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import cz.uhk.ppro.dto.CourseWithCount;
import cz.uhk.ppro.model.Category;
import cz.uhk.ppro.model.Test;
import cz.uhk.ppro.model.User;
import cz.uhk.ppro.model.UserWord;
import cz.uhk.ppro.model.Word;

@Controller
@RequestMapping("/courses/")
public class CoursesController extends AbstractController {
	private final String BASE_VIEW = "courses/";
	private final static String NOW = "now";

	@RequestMapping(value = { "/" }, method = RequestMethod.GET)
	public String listCourses(ModelMap model) {
		List<CourseWithCount> courses = testService.findAllTestsWithUserCounts();
		model.addAttribute("courses", courses);
		List<Category> categories = categoryService.findAllCategories();
		model.addAttribute("categories", categories);
		return BASE_VIEW + "all";
	}

	@RequestMapping(value = { "/category/{url}" }, method = RequestMethod.GET)
	public String coursesByTitle(@PathVariable String url, ModelMap model,
			RedirectAttributes redirectAttributes, HttpServletResponse response) {
		Category category = categoryService.findCategoryByUrl(url);
		if (category == null) {
			redirectAttributes.addFlashAttribute("error", "Category was not found!");
			return "redirect:/courses/";
		} else {
			List<CourseWithCount> courses = testService.findAllTestsByCategoryWithUserCounts(category);
			model.addAttribute("courses", courses);
			List<Category> categories = categoryService.findAllCategories();
			model.addAttribute("categories", categories);
			model.addAttribute("current_category", category);
			return BASE_VIEW + "category";
		}
	}

	@RequestMapping(value = { "/detail/{id_test}" }, method = RequestMethod.GET)
	public String courseDetail(@PathVariable int id_test, ModelMap model, RedirectAttributes redirectAttributes) {
		Test test = testService.findTestById(id_test);
		if (test == null) {
			redirectAttributes.addFlashAttribute("error", "Course was not found!");
			return "redirect:/courses/";
		} else {
			// test
			model.addAttribute("test", test);

			// user in test
			User user = getCurrentUser();
			model.addAttribute("user", user);
			boolean userInTest = userTestService.isUserInTest(user, test);
			model.addAttribute("userInTest", userInTest);

			//words
			List<Word> words = wordService.findAllWordsInTest(test);
			if (userInTest) {
				boolean somethingToReview = false;
				List<UserWord> userWords = userWordService.findAllUserWordsByUserAndWords(user, words);
				for (UserWord userWord : userWords) {
					userWord.setNextRepetitionString(parseDateTime(userWord.getNext_repetition()));
					if (userWord.getNextRepetitionString() == NOW) {
						somethingToReview = true;
					}
				}
				model.addAttribute("userWords", userWords);
				model.addAttribute("somethingToReview", somethingToReview);
			} else {
				model.addAttribute("words", words);
			}
			return BASE_VIEW + "detail";
		}
	}

	@RequestMapping(value = { "/enroll/{id_test}" }, method = RequestMethod.GET)
	public String enrollCourse(@PathVariable int id_test, ModelMap model, RedirectAttributes redirectAttributes) {
		Test test = testService.findTestById(id_test);
		if (test == null) {
			redirectAttributes.addFlashAttribute("error", "Course was not found!");
			return "redirect:/courses/";
		} else {
			User user = getCurrentUser();
			addUserToTest(user, test);
			redirectAttributes.addFlashAttribute("success", "You were successfully enrolled to course " + test.getTitle() + ".");
			return "redirect:/courses/detail/" + test.getId_test();
		}
	}

	private String parseDateTime(LocalDateTime localDateTime) {
		DateTime now = new DateTime();
		DateTime repetition = localDateTime.toDateTime();

		if (repetition.isBeforeNow()) {
			return NOW;
		} else {
			Duration duration = new Duration(now, repetition);
			Long days = duration.getStandardDays();
			Long hours = duration.getStandardHours();
			Long minutes = duration.getStandardMinutes();
			Long seconds = duration.getStandardSeconds();
			Long millis = duration.getMillis();
			if (days > 1) {
				return days.toString() + " days";
			} else if (days == 1) {
				return days.toString() + " day";
			} else if (hours > 1) {
				return hours.toString() + " hours";
			} else if (hours == 1) {
				return hours.toString() + " hour";
			} else if (minutes > 1) {
				return minutes.toString() + " minutes";
			} else if (minutes == 1) {
				return minutes.toString() + " minute";
			} else if (seconds > 1) {
				return seconds.toString() + " seconds";
			} else if (seconds == 1) {
				return seconds.toString() + " second";
			} else if (millis > 1) {
				return millis.toString() + " milliseconds";
			} else if (millis == 1) {
				return millis.toString() + " millisecond";
			} else {
				return "You shall not pass!";
			}
		}

	}

}