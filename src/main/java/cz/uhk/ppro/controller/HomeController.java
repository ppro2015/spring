package cz.uhk.ppro.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cz.uhk.ppro.dto.CourseItem;
import cz.uhk.ppro.model.User;
import cz.uhk.ppro.model.UserTest;

@Controller
@RequestMapping("/")
public class HomeController extends AbstractController {

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String defaultPage() {
		return "redirect:/home/";
	}

	@RequestMapping(value = "/home/", method = RequestMethod.GET)
	public String homePage(ModelMap model) {
		User user = getCurrentUser();

		model.addAttribute("user", user);
		userService.updateUserDailyDone(user);
		int wordsCount = userWordService.findAllUserWordsByUser(user).size();
		model.addAttribute("wordsCount", wordsCount);

		List<UserTest> userTests = userTestService.findAllUserTestByUser(user);
		List<CourseItem> courseItems = new ArrayList<>(userTests.size());
		for (UserTest userTest : userTests) {
			CourseItem item = new CourseItem();
			item.setId(userTest.getTest().getId_test());
			item.setPicture(userTest.getTest().getPicture());
			item.setTitle(userTest.getTest().getTitle());
			item.setCountForReview(userWordService.findAllUserWordsForReview(user, userTest.getTest()).size());
			courseItems.add(item);
		}
		model.addAttribute("courseItems", courseItems);

		return "home/index";
	}
}