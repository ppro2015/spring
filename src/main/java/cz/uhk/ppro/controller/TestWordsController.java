package cz.uhk.ppro.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import cz.uhk.ppro.model.Test;
import cz.uhk.ppro.model.Word;

@Controller
@RequestMapping("/test/")
public class TestWordsController extends AbstractController {
	private final String BASE_VIEW = "words/";

	@RequestMapping(value = { "/{id_test}/words" }, method = RequestMethod.GET)
	public String listWords(@PathVariable int id_test, ModelMap model, RedirectAttributes redirectAttributes) {
		Test test = testService.findTestById(id_test);
		if (test == null) {
			redirectAttributes.addFlashAttribute("error", "Test was not found.");
			return "redirect:/tests/";
		} else {
			List<Word> words = wordService.findAllWordsInTest(test);
			model.addAttribute("words", words);
			model.addAttribute("test", test);
			return BASE_VIEW + "all";
		}
	}

	@RequestMapping(value = { "/{id_test}/new-word" }, method = RequestMethod.GET)
	public String newWord(@PathVariable int id_test, ModelMap model, RedirectAttributes redirectAttributes) {
		Test test = testService.findTestById(id_test);
		if (test == null) {
			redirectAttributes.addFlashAttribute("error", "Test was not found.");
			return "redirect:/tests/";
		} else {
			Word word = new Word();
			word.setTest(testService.findTestById(id_test));
			model.addAttribute("word", word);
			return BASE_VIEW + "new";
		}
	}

	@RequestMapping(value = { "/{id_test}/new-word" }, method = RequestMethod.POST)
	public String saveWord2(@PathVariable int id_test, @Valid Word word, BindingResult result,
			ModelMap model, RedirectAttributes redirectAttributes) {

		Test test = testService.findTestById(id_test);
		if (test == null) {
			redirectAttributes.addFlashAttribute("error", "Test was not found.");
			return "redirect:/tests/";
		}

		if (result.hasErrors()) {
			return BASE_VIEW + "new";
		}

		saveWord(word, test);

		redirectAttributes.addFlashAttribute("success", "Word '" + word.getAnswer() + "' was successfully created.");
		return "redirect:/test/" + id_test + "/words";
	}

	@RequestMapping(value = { "/{id_test}/edit-word/{id_word}" }, method = RequestMethod.GET)
	public String editWord(@PathVariable int id_test, @PathVariable int id_word, ModelMap model,
			RedirectAttributes redirectAttributes) {
		Test test = testService.findTestById(id_test);
		Word word = wordService.findWordById(id_word);
		if (test == null) {
			redirectAttributes.addFlashAttribute("error", "Test was not found!");
			return "redirect:/tests/";
		} else if (word == null) {
			redirectAttributes.addFlashAttribute("error", "Word was not found!");
			return "redirect:/test/" + id_test + "/words";
		} else {
			model.addAttribute("word", word);
			model.addAttribute("test", test);
			return BASE_VIEW + "edit";
		}
	}

	@RequestMapping(value = { "/{id_test}/edit-word/{id_word}" }, method = RequestMethod.POST)
	public String updateWord(@PathVariable int id_test, @PathVariable int id_word, @Valid Word word,
			BindingResult result, ModelMap model, RedirectAttributes redirectAttributes) {

		Test test = testService.findTestById(id_test);
		Word word1 = wordService.findWordById(id_word);
		if (test == null) {
			redirectAttributes.addFlashAttribute("error", "Test was not found!");
			return "redirect:/tests/";
		} else if (word1 == null) {
			redirectAttributes.addFlashAttribute("error", "Word was not found!");
			return "redirect:/test/" + id_test + "/words";
		} else {
			if (result.hasErrors()) {
				return BASE_VIEW + "edit";
			}

			wordService.updateWord(word);

			redirectAttributes.addFlashAttribute("success", "Word '" + word.getKey() + "' was successfully updated.");
			return "redirect:/test/" + id_test + "/words";
		}
	}
}