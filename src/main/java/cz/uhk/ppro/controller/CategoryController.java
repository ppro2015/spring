package cz.uhk.ppro.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import cz.uhk.ppro.common.Utils;
import cz.uhk.ppro.model.Category;
import cz.uhk.ppro.model.Test;

@Controller
@RequestMapping("/categories/")
public class CategoryController extends AbstractController {
	private final String BASE_VIEW = "categories/";

	@RequestMapping(value = { "/" }, method = RequestMethod.GET)
	public String listCategories(ModelMap model) {
		List<Category> categories = categoryService.findAllCategories();
		model.addAttribute("categories", categories);
		return BASE_VIEW + "all";
	}

	@RequestMapping(value = { "/new" }, method = RequestMethod.GET)
	public String newCategory(ModelMap model) {
		Category category = new Category();
		model.addAttribute("category", category);
		model.addAttribute("parent_category", getCategoriesMap(0)); //��dn� kategorie nem� id=0
		return BASE_VIEW + "new";
	}

	@RequestMapping(value = { "/new" }, method = RequestMethod.POST)
	public String saveCategory(@Valid Category category, BindingResult result, ModelMap model, RedirectAttributes redirectAttributes) {

		checkCategoryUniqueness(category, result);

		if (category.getId_parent().getId_category() > 0) {
			if (!categoryService.isCategoryParent(category.getId_parent())) {
				FieldError titleError = new FieldError("category", "id_parent.id_category", "Parent category cannot have parent.");
				result.addError(titleError);
			}
		}

		if (result.hasErrors()) {
			model.addAttribute("parent_category", getCategoriesMap(0)); //��dn� kategorie nem� id_0
			return BASE_VIEW + "new";
		}

		categoryService.saveCategory(category);

		redirectAttributes.addFlashAttribute("success", "Category '" + category.getTitle() + "' was insert successfully.");
		return "redirect:/" + BASE_VIEW;
	}

	@RequestMapping(value = { "/edit/{id_category}" }, method = RequestMethod.GET)
	public String editCategory(@PathVariable int id_category, ModelMap model, RedirectAttributes redirectAttributes) {
		Category category = categoryService.findCategoryById(id_category);
		if (category == null) {
			redirectAttributes.addFlashAttribute("error", "Category was not found!");
			return "redirect:/" + BASE_VIEW;
		} else {
			model.addAttribute("category", category);
			model.addAttribute("parent_category", getCategoriesMap(id_category));
			return BASE_VIEW + "edit";
		}
	}

	@RequestMapping(value = { "/edit/{id_category}" }, method = RequestMethod.POST)
	public String updateCategory(@PathVariable int id_category, @Valid Category category,
			BindingResult result, ModelMap model, RedirectAttributes redirectAttributes) {

		checkCategoryUniqueness(category, result);

		if (category.getId_parent().getId_category() != 0) {
			if (!categoryService.isCategoryParent(category.getId_parent())) {
				FieldError titleError = new FieldError("category", "id_parent.id_category", "Parent category cannot have parent.");
				result.addError(titleError);

			}
			if (id_category == category.getId_parent().getId_category()) {
				FieldError titleError = new FieldError("category", "id_parent.id_category", "Category cannot be its own parent.");
				result.addError(titleError);
			}
		}

		if (result.hasErrors()) {
			model.addAttribute("parent_category", getCategoriesMap(id_category));
			return BASE_VIEW + "edit";
		}

		categoryService.updateCategory(category);

		redirectAttributes.addFlashAttribute("success", "Category '" + category.getTitle() + "' was successfully updated.");
		return "redirect:/" + BASE_VIEW;
	}

	@RequestMapping(value = { "/delete/{id_category}" }, method = RequestMethod.GET)
	public String deleteCategory(@PathVariable int id_category, ModelMap model, RedirectAttributes redirectAttributes) {
		Category category = categoryService.findCategoryById(id_category);
		if (category == null) {
			redirectAttributes.addFlashAttribute("error", "Category was not found!");
			return "redirect:/" + BASE_VIEW;
		} else {
			model.addAttribute("category", category);
			return BASE_VIEW + "delete";
		}
	}

	@RequestMapping(value = { "/delete/{id_category}" }, method = RequestMethod.POST)
	public String reallyDeleteCategory(@PathVariable int id_category, ModelMap model, RedirectAttributes redirectAttributes) {
		Category category = categoryService.findCategoryById(id_category);
		if (category == null) {
			redirectAttributes.addFlashAttribute("error", "Category was not found!");
		} else {
			List<Test> tests = testService.findAllTestsByCategory(category);
			if (tests.size() > 0) {
				redirectAttributes.addFlashAttribute("error", "You cannot delete category that has assigned tests!");
			} else {
				categoryService.deleteCategory(category);
				redirectAttributes.addFlashAttribute("success", "Category '" + category.getTitle() + "' was successfully deleted.");
			}
		}
		return "redirect:/" + BASE_VIEW;
	}

	@RequestMapping(value = "/ajax-delete/{id_category}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<String> reallyDeleteCategoryAjax(@PathVariable int id_category) {
		Category category = categoryService.findCategoryById(id_category);
		if (category == null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Category was not found!");
		} else {
			List<Test> tests = testService.findAllTestsByCategory(category);
			if (tests.size() > 0) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("You cannot delete category that has assigned tests!");
			} else {
				categoryService.deleteCategory(category);
				return ResponseEntity.ok("");
			}
		}
	}

	private Map<Integer, String> getCategoriesMap(int id_category) {
		List<Category> categories = categoryService.findAllNonParentCategories();
		Map<Integer, String> categoriesMap = new HashMap<Integer, String>();
		for (int i = 0; i < categories.size(); i++) {
			if (id_category != categories.get(i).getId_category()) {
				categoriesMap.put(categories.get(i).getId_category(), categories.get(i).getTitle());
			}
		}
		return categoriesMap;
	}

	private void checkCategoryUniqueness(Category category, BindingResult result) {
		if (!categoryService.isCategoryTitleUnique(category.getId_category(), category.getTitle())) {
			FieldError titleError = new FieldError("category", "title",
					"Category with title " + category.getTitle() + " already exists. Please choose different one.");
			result.addError(titleError);
		}
		String url = Utils.urlifyString(category.getTitle());
		if (!categoryService.isCategoryUrlUnique(category.getId_category(), url)) {
			FieldError titleError = new FieldError("category", "title",
					"Category with url " + url + " already exists. Please choose different title.");
			result.addError(titleError);
		}
	}

}