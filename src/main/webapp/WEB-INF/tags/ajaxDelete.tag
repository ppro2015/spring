<%@ tag body-content="tagdependent" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="what_delete" required="true" rtexprvalue="true" description="Text describing what to delete"%>
<%@ attribute name="a_class" required="true" rtexprvalue="true" description="Class of clickable href"%>
<%@ attribute name="url" required="true" rtexprvalue="true" description="URL for ajax request"%>
<%@ attribute name="on_success" required="false" rtexprvalue="true" description="Key for c:choose for inserting custom code when ajax is succesfull"%>

<%-- Mazání položek z tabulky ajaxem --%>
<script>
$(document).ready(function() {
	function ajaxDelete() {
		$(".${a_class}").click(function() {
			var id = $(this).parent().parent().children().first().text();
			swal({
				title: "Delete ${what_delete}",
				text: "Are you sure you want to delete this ${what_delete}?",
				type: "warning",
				showCancelButton: true,
				closeOnConfirm: false,
				showLoaderOnConfirm: true, 
			}, function() {
				$.get("/SpringProject/${url}/"+id, function(data) {
					<c:choose>
						<c:when test="${on_success == 'show-users'}">
							var nick = $("#table").bootstrapTable('getRowByUniqueId', id)["nickname"];
							$("select").append('<option value="'+id+'">'+nick+'</option>');
						</c:when>
					</c:choose>
					$("#table").bootstrapTable('remove', {field: 'id', values: [id]});
					swal({
						title: "Deleted!",
						type: "success"
					});
				}).fail(function(msg) {
					if (msg.responseText != "") {
						swal({
							title: "Error! " + msg.status,
							text: msg.responseText,
							type: "error"
						});
					} else {
						swal({
							title: "Error! " + msg.status,
							text: "Sorry. Something went wrong. That's all we know.",
							type: "error"
						});
					}
				});
			});
			return false;
		});
	}
	$("#table").on("post-body.bs.table, all.bs.table", ajaxDelete);
	ajaxDelete();

});

</script>