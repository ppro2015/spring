<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>

<tiles:insertDefinition name="mainTemplate">
<tiles:putAttribute name="title" value="Access denied" />
<tiles:putAttribute name="body">

<div class="text-center">
	<h1>HTTP Status 403 - Forbidden</h1>
	<h2>Access denied</h2>
	<h3>You do not have permission to access this page!</h3>
	<img src="<c:url value="/img/403.jpg" />">
</div>

</tiles:putAttribute>
</tiles:insertDefinition>
