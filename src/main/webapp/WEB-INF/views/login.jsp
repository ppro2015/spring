<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<tiles:insertDefinition name="loginTemplate">
	<tiles:putAttribute name="title" value="Login Page" />
	<tiles:putAttribute name="body">

	<div class="center-block row">
		<div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">

		<h2>Please sign in</h2>

		<c:if test="${not empty error}">
			<div class="alert alert-danger">${error}</div>
		</c:if>
		<c:if test="${not empty msg}">
			<div class="alert alert-success">${msg}</div>
		</c:if>

		<form name="loginForm" class="form-horizontal"
			action="<c:url value='/login' />" method='POST'>

			<div class="form-group">
				<label for="nickname" class="control-label col-xs-2">Nickname</label>
				<div class="col-xs-10">
					<input type="text" name="nickname" value="" class="form-control" placeholder="Nickname">
				</div>
			</div>

			<div class="form-group">
				<label for="password" class="control-label col-xs-2">Password</label>
				<div class="col-xs-10">
					<input type="password" name="password" value="" class="form-control" placeholder="Password">
				</div>
			</div>

			<div class="form-group">
				<div class="col-xs-offset-2 col-xs-10">
					<button type="submit" class="btn btn-primary btn-lg">Login</button>
				</div>
			</div>
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
		</form>
		</div>
	</div>
</tiles:putAttribute>
</tiles:insertDefinition>