<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>

<tiles:insertDefinition name="mainTemplate">
<tiles:putAttribute name="title" value="Tests settings" />
<tiles:putAttribute name="body">

<div class="settings-page">
	<h2>List of studied tests</h2>
	<table class="table table-nobordered"
		id="table"
		data-toggle="table"
		data-unique-id="id"
		data-mobile-responsive="true">
	<thead>
	<tr>
		<th data-field="id" class="hidden-lg hidden-md hidden-sm hidden-xs">#</th>
		<th data-field="title">Title</th>
		<th>Remove</th>
	</tr>
	</thead>
	<tbody>
		<c:forEach items="${userTests}" var="userTest">
			<tr>
			<td>${userTest.test.id_test}</td>
			<td>${userTest.test.title}</td>
			<td><a class="ajax-delete-test ajax-href">Remove test</a></td>
			</tr>
		</c:forEach>
	</tbody>
	</table>
</div>
</tiles:putAttribute>

<tiles:putAttribute name="custom-scripts">
	<tag:ajaxDelete what_delete="test" a_class="ajax-delete-test" url="settings/tests/ajax-delete" />
</tiles:putAttribute>
</tiles:insertDefinition>