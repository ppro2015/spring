<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>

<tiles:insertDefinition name="mainTemplate">
<tiles:putAttribute name="title" value="Basic settings" />
<tiles:putAttribute name="body">

<div class="settings-page">
	<div class="row" style="margin: 15px;">
		<div class="col-sm-6 col-xs-12">
			<c:url var="daily_goal_url" value="/settings/basic/daily-goal" />
			<form:form method="POST" action="${daily_goal_url}" modelAttribute="user">
				<tag:inputField name="daily_goal" label="Daily goal" type="text" />
				<button type="submit" class="btn btn-lg btn-primary">Update daily goal</button>
			</form:form>
		</div>
		<div class="col-sm-6 col-xs-12">
			<c:url var="password_url" value="/settings/basic/password" />
			<form:form method="POST" action="${password_url}" modelAttribute="user">
				<tag:inputField name="password" label="Password" type="password"/>
				<tag:inputField name="confirmPassword" label="Confirm Password" type="password"/>
				<button type="submit" class="btn btn-lg btn-primary">Update password</button>
			</form:form>
		</div>
	</div>
</div>

</tiles:putAttribute>
</tiles:insertDefinition>