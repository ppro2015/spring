<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>

<tiles:insertDefinition name="mainTemplate">
<tiles:putAttribute name="title" value="Dashboard" />
<tiles:putAttribute name="body">

<div class="home-page row">
	<div class="col-md-3">
		<div class="user-info-box">
			<div class="header">Info</div>
			<div class="nickname">${user.nickname}</div>
			<div class="row">
				<div class="col-xs-6 value"><fmt:formatNumber type="number" value="${wordsCount}" /></div>
				<div class="col-xs-6 value"><fmt:formatNumber type="number" value="${user.points}" /></div>
				<div class="col-xs-6 value-label">words</div>
				<div class="col-xs-6 value-label">points</div>
			</div>
		</div>
		<div class="user-info-box">
			<div class="header">Daily goal</div>
			<div class="goal-stats">${user.daily_done}<span class="out-of"> done out of </span>${user.daily_goal}</div>
			<c:choose>
				<c:when test="${user.daily_done >= user.daily_goal}">
					<div class="goal achieved">Daily goal achieved.</div>
				</c:when>
				<c:otherwise>
					<div class="goal nonachieved">Daily goal not achieved yet.</div>
				</c:otherwise>
			</c:choose>
		</div>
	</div>
	<div class="col-md-9">
		<c:if test="${fn:length(courseItems) == 0}">
			<h3 class="text-center">You are not enrolled to any course.</h3>
		</c:if>
		<c:forEach items="${courseItems}" var="courseItem">
		<div class="test-item">
			<img height="120" src="${courseItem.picture}">
			<div class="course-box">
				<a href="<c:url value='/courses/detail/${courseItem.id}' />">${courseItem.title}</a><br>
				<c:choose>
					<c:when test="${courseItem.countForReview == 0}">
						<span class="review-words-count">Words for review: 0</span><br>
						<a class="btn btn-info btn-lg review-button disabled">Review words</a>
					</c:when>
					<c:otherwise>
						<span class="review-words-count">Words for review: ${courseItem.countForReview}</span><br>
						<a class="btn btn-info btn-lg review-button" href="<c:url value='/courses/review/${courseItem.id}' />">Review words</a>
					</c:otherwise>
				</c:choose>
			</div>
			<div class="clearfix"></div>
		</div>
		</c:forEach>
	</div>
</div>

</tiles:putAttribute>
</tiles:insertDefinition>