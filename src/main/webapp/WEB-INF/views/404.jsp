<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>

<tiles:insertDefinition name="mainTemplate">
<tiles:putAttribute name="title" value="Page not found" />
<tiles:putAttribute name="body">

<div class="text-center">
	<h1>404 - Not Found</h1>
	<h3>The page you were looking for was not found.</h3>
</div>

</tiles:putAttribute>
</tiles:insertDefinition>
