<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>

<tiles:insertDefinition name="superUserTemplate">
<tiles:putAttribute name="title" value="List of all categories" />
<tiles:putAttribute name="body">

	<h2>List of all categories</h2>
	<p><a href="<c:url value='/categories/new' />">Add New Category</a></p>

	<jsp:include page="../table-settings.jsp" />
	<thead>
	<tr>
		<th data-field="id" data-sortable="true">#</th>
		<th data-field="title" data-sortable="true">Title</th>
		<th data-field="description" data-sortable="true">Description</th>
		<th data-field="id_parent" data-sortable="true">Parent category</th>
		<th data-searchable="false">&nbsp;</th>
		<th data-searchable="false">&nbsp;</th>
	</tr>
	</thead>
	<tbody>
		<c:forEach items="${categories}" var="category">
			<tr>
			<td>${category.id_category}</td>
			<td>${category.title}</td>
			<td>${category.description}</td>
			<td>${category.id_parent.title}</td>
			<td><a data-placement="top" data-toggle="tooltip" title="Edit"
					class="btn btn-primary btn-xs" href="<c:url value='/categories/edit/${category.id_category}' />">
					<span class="glyphicon glyphicon-pencil"></span>
			</a></td>
			<td><a data-placement="top" data-toggle="tooltip" title="Delete"
					class="btn btn-danger btn-xs ajax-delete-category" href="<c:url value='/categories/delete/${category.id_category}' />">
					<span class="glyphicon glyphicon-trash"></span>
			</a></td>
			</tr>
		</c:forEach>
	</tbody>
	</table>
</tiles:putAttribute>

<tiles:putAttribute name="custom-scripts">
	<tag:ajaxDelete what_delete="category" a_class="ajax-delete-category" url="categories/ajax-delete"/>
	<tag:tooltipInit/>
</tiles:putAttribute>

</tiles:insertDefinition>
