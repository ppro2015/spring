<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<tiles:insertDefinition name="superUserTemplate">
<tiles:putAttribute name="title" value="Delete category" />
<tiles:putAttribute name="body">

	<h2>Delete category</h2>
	<p>Are you sure you want to completely delete user <i>${user.nickname}</i>?</p>
	<form:form method="POST">
		<button type="submit" class="btn btn-lg btn-warning">Delete</button>
	</form:form>
	<br/>
	<br/>
	<a href="<c:url value='/categories/' />">Go back to list of all categories</a>

</tiles:putAttribute>
</tiles:insertDefinition>