<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<tiles:insertDefinition name="superUserTemplate">
<tiles:putAttribute name="title" value="Delete user from tests" />
<tiles:putAttribute name="body">

	<h2>Delete user <i>${user.nickname}</i> from test <i>${test.title}</i></h2>
	<p>Are you sure?</p>
	<form:form method="POST">
		<button type="submit" class="btn btn-lg btn-warning">Delete</button>
	</form:form>
	<br/>
	<br/>
	<a href="<c:url value='/show-users/${test.id_test}' />">Go back to list of all users for this test</a>

</tiles:putAttribute>
</tiles:insertDefinition>
