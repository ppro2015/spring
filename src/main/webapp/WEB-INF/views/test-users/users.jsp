<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>

<tiles:insertDefinition name="superUserTemplate">
<tiles:putAttribute name="title" value="List of users in test" />
<tiles:putAttribute name="body">

	<h2>List of all users in test <i>${test.title}</i></h2>
	<a href="<c:url value='/tests/' />">Go back to list of all tests</a>

	<c:if test="${not empty newUsers}">
		<form:form method="POST" modelAttribute="user">
			<div class="row" style="margin: 15px;">
				<div class="col-md-7 col-xs-12">
					<tag:inputField name="id_user" label="Add user" type="select" data="${newUsers}" />
					<button type="submit" class="btn btn-lg btn-primary ajax-add-test-user">Add</button>
				</div>
			</div>
		</form:form>
	</c:if>

	<jsp:include page="../table-settings.jsp" />
	<thead>
	<tr>
		<th data-field="id" data-sortable="true">#</th>
		<th data-field="nickname" data-sortable="true">Nickname</th>
		<th data-field="del" data-searchable="false">&nbsp;</th>
	</tr>
	</thead>
	<tbody>
			<c:forEach items="${test_users}" var="test_user">
				<tr>
				<td>${test_user.user.id_user}</td>
				<td><a href="<c:url value='/users/edit/${test_user.user.id_user}' />">${test_user.user.nickname}</a></td>
				<td><a data-placement="top" data-toggle="tooltip" title="Delete"
					class="btn btn-danger btn-xs ajax-delete-test-user" href="<c:url value='/test/${test.id_test}/delete-user/${test_user.user.id_user}' />">
					<span class="glyphicon glyphicon-trash"></span>
				</a></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</tiles:putAttribute>

<tiles:putAttribute name="custom-scripts">
	<c:set var="a_class" scope="session" value="ajax-delete-test-user"/>
	<c:set var="url" scope="session" value="tests/ajax-delete-user/${test.id_test}"/>

	<tag:ajaxDelete what_delete="user from this test" a_class="ajax-delete-test-user"
					url="test/${test.id_test}/ajax-delete-user" on_success="show-users"/>
	<tag:tooltipInit/>
<script>
$(document).ready(function() {	
	$(".ajax-add-test-user").click(function() {
		var option = $("select option:selected");
		if (option.length == 0) {
			swal({
				title: "Error!",
				text: "You have to select a user to add.",
				type: "warning"
			});
			return false;
		}
		var u_nick = option.text();
		var u_id = option.attr("value");
		$.get("/SpringProject/test/${test.id_test}/ajax-add-user/"+u_id, function(data) {
			$("#table").bootstrapTable('append', {
				id:u_id,
				nickname:'<a href="/SpringProject/users/edit/'+u_id+'">'+u_nick+'</a>',
				del:'<a data-placement="top" data-toggle="tooltip" title="Delete" class="btn btn-danger btn-xs ajax-delete-test-user" href="/SpringProject/tests/delete-user/"><span class="glyphicon glyphicon-trash"></span></a>'
			});
			option.remove();
			swal({
				title: "Added!",
				text: "User was successfully added.",
				type: "success"
			});
		}).fail(function(msg) {
			if (msg.responseText != "") {
				swal({
					title: "Error! " + msg.status,
					text: msg.responseText,
					type: "error"
				});
			} else {
				swal({
					title: "Error! " + msg.status,
					text: "Sorry. Something went wrong. That's all we know.",
					type: "error"
				});
			}
		});
		return false;
	});
});
</script>
</tiles:putAttribute>
</tiles:insertDefinition>
