<script type="text/javascript" src="/SpringProject/js/jquery-1.12.0.min.js"></script>
<script type="text/javascript" src="/SpringProject/js/bootstrap.min.js"></script>

<script type="text/javascript" src="/SpringProject/js/bootstrap-table.min.js"></script>
<script type="text/javascript" src="/SpringProject/js/bootstrap-table-export.min.js"></script>
<script type="text/javascript" src="/SpringProject/js/tableExport.min.js"></script>
<script type="text/javascript" src="/SpringProject/js/bootstrap-table-mobile.min.js"></script>
<script type="text/javascript" src="/SpringProject/js/bootstrap-table-toolbar.min.js"></script>

<script type="text/javascript" src="/SpringProject/js/sweetalert.min.js"></script>
<script type="text/javascript" src="/SpringProject/js/float-label-form.js"></script>
<script>
$(document).ready(function() {
	var menuTimerId;
	$("li.dropdown").mouseover(function() {
		var self = $(this);
		clearTimeout(menuTimerId);
		menuTimerId = setTimeout(function() {
			self.addClass("open");
		}, 50);
	});
	$("li.dropdown").mouseout(function() {
		var self = $(this);
		clearTimeout(menuTimerId);
		menuTimerId = setTimeout(function() {
			self.removeClass("open");	
		}, 250);
	});

});
</script>
