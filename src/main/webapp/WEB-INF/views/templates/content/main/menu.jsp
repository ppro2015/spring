<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<c:set var="url" value="${fn:replace(requestScope['javax.servlet.forward.request_uri'],'/SpringProject/','')}"/>
<nav class="navigation container">
	<span class="pull-left logo"><img src="/SpringProject/img/logo.png" alt="FIM Dictionary"></span>
	<%-- fantastic incredible memorizer :) --%>
	<sec:authorize access="isAuthenticated()">
		<div class="pull-right user-info visible-xs">
			<span class="user">${pageContext.request.userPrincipal.name}</span> | <a href="<c:url value="/logout" />">Logout</a>
		</div>
	</sec:authorize>
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" 
			data-target="#menu-navbar-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
	</div>
	<ul class="collapse nav navbar-nav navbar-collapse" id="menu-navbar-collapse">
		<li<c:if test="${fn:startsWith(url, 'home')}"> class="active"</c:if>><a href="<c:url value='/home/' />">Home</a></li>
		<li<c:if test="${fn:startsWith(url, 'courses')}"> class="active"</c:if>><a href="<c:url value='/courses/' />">Courses</a></li>
		<li class="dropdown <c:if test="${fn:startsWith(url, 'settings')}"> active</c:if>">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown">Settings <b class="caret"></b></a>
			<ul class="dropdown-menu">
				<li<c:if test="${fn:startsWith(url, 'settings/basic')}"> class="active"</c:if>><a href="<c:url value='/settings/basic/' />">Basic</a></li>
				<li<c:if test="${fn:startsWith(url, 'settings/tests')}"> class="active"</c:if>><a href="<c:url value='/settings/tests/' />">Tests</a></li>
			</ul>
		</li>
		<sec:authorize access="hasRole('ROLE_ADMIN')">
			<li<c:if test="${fn:startsWith(url, 'categories')}"> class="active"</c:if>><a href="<c:url value='/categories/' />">Categories</a></li>
		</sec:authorize>
	</ul>
	<sec:authorize access="isAuthenticated()">
		<div class="pull-right user-info hidden-xs">
			<span class="user">${pageContext.request.userPrincipal.name}</span> | <a href="<c:url value="/logout" />">Logout</a>
		</div>
	</sec:authorize>
	<div class="clearfix"></div>
</nav>
