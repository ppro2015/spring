<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<h1 class="pull-left">FIM Dictionary</h1>
<%-- fantastic incredible memorizer :) --%>
<sec:authorize access="isAuthenticated()">
<div class="pull-right">
	<span class="user">${pageContext.request.userPrincipal.name}</span> | <a href="<c:url value="/logout" />">Logout</a>
</div>
</sec:authorize>
<div class="clearfix"></div>
