<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<c:set var="url" value="${fn:replace(requestScope['javax.servlet.forward.request_uri'],'/SpringProject/','')}"/>

<nav class="navbar navbar-default" role="navigation">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" 
			data-target="#menu-navbar-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
	</div>
	<div class="collapse navbar-collapse" id="menu-navbar-collapse">
		<ul class="nav navbar-nav">
			<sec:authorize access="hasRole('ROLE_SUPERUSER')">
				<li<c:if test="${fn:startsWith(url, 'users')}"> class="active"</c:if>><a href="<c:url value='/users/' />">Users</a></li>
				<li<c:if test="${fn:startsWith(url, 'test')}"> class="active"</c:if>><a href="<c:url value='/tests/' />">Tests</a></li>
				<li<c:if test="${fn:startsWith(url, 'categories')}"> class="active"</c:if>><a href="<c:url value='/categories/' />">Categories</a></li>
				<li<c:if test="${fn:startsWith(url, 'words')}"> class="active"</c:if>><a href="<c:url value='/words/' />">Words</a></li>
			</sec:authorize>
			<sec:authorize access="hasRole('ROLE_ADMIN')">
				<li><a href="<c:url value='/home/' />">Home</a></li>
				<li><a href="<c:url value='/courses/' />">Courses</a></li>
				<li><a href="<c:url value='/settings/' />">Settings</a></li>
				<li<c:if test="${fn:startsWith(url, 'categories')}"> class="active"</c:if>><a href="<c:url value='/categories/' />">Categories</a></li>
			</sec:authorize>
		</ul>
	</div>
</nav>
