<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<!DOCTYPE html>
<html lang="cs-cz" dir="ltr">
<head>
	<meta charset="UTF-8">
	<title><tiles:insertAttribute name="title" /></title>
	<tiles:insertAttribute name="styles" />
</head>

<body>
<div class="container">
	<div class="head">
	<tiles:insertAttribute name="header" />
	</div>
	<div class="content">
	<tiles:insertAttribute name="body" />
	</div>
	<tiles:insertAttribute name="footer" />
</div>
</body>
</html>