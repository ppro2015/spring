<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<!DOCTYPE html>
<html lang="cs-cz" dir="ltr">
<head>
	<meta charset="UTF-8">
	<title><tiles:insertAttribute name="title" /></title>
	<tiles:insertAttribute name="styles" />
</head>

<body>
<div class="head">
	<tiles:insertAttribute name="menu" />
</div>
<div class="container">
	<div class="content">
		<tiles:insertAttribute name="messages" />
		<tiles:insertAttribute name="body" />
	</div>
	<tiles:insertAttribute name="footer" />
</div>
<tiles:insertAttribute name="scripts" />
<tiles:insertAttribute name="custom-scripts" />
</body>
</html>