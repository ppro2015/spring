	<table class="table table-nobordered"
		id="table"
		data-toggle="table"
		data-unique-id="id"

		data-pagination="true"
		data-page-list="[5, 10, 20, 50, 100, 200]"
		data-search="true"

		data-show-columns="true"
		data-show-pagination-switch="true"
		data-show-export="true"

		data-mobile-responsive="true"
		data-check-on-init="true"

		data-advanced-search="true"
		data-id-table="advancedTable">