<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>

<tiles:insertDefinition name="superUserTemplate">
<tiles:putAttribute name="title" value="List of all words" />
<tiles:putAttribute name="body">

	<h2>List of all words</h2>
	<noscript>Found ${fn:length(words)} words.</noscript>

	<jsp:include page="../table-settings.jsp" />
	<thead>
	<tr>
		<th data-field="id" data-sortable="true">#</th>
		<th data-field="key" data-sortable="true">Key</th>
		<th data-field="answer" data-sortable="true">Answer</th>
		<th data-field="test" data-sortable="true">Test</th>
		<th data-searchable="false">&nbsp;</th>
		<th data-searchable="false">&nbsp;</th>
	</tr>
	</thead>
	<tbody>
		<c:forEach items="${words}" var="word">
			<tr>
			<td>${word.id_word}</td>
			<td>${word.key}</td>
			<td>${word.answer}</td>
			<td><a href="<c:url value='/tests/edit/${word.test.id_test}' />">${word.test.title}</a></td>
			<td><a data-placement="top" data-toggle="tooltip" title="Edit"
					class="btn btn-primary btn-xs" href="<c:url value='/test/${word.test.id_test}/edit-word/${word.id_word}' />">
					<span class="glyphicon glyphicon-pencil"></span>
			</a></td>
			<td><a data-placement="top" data-toggle="tooltip" title="Delete"
					class="btn btn-danger btn-xs ajax-delete-word" href="<c:url value='/words/delete/${word.id_word}' />">
					<span class="glyphicon glyphicon-trash"></span>
			</a></td>
			</tr>
		</c:forEach>
	</tbody>
	</table>
</tiles:putAttribute>

<tiles:putAttribute name="custom-scripts">
	<tag:ajaxDelete what_delete="word" a_class="ajax-delete-word" url="words/ajax-delete" />
	<tag:tooltipInit/>
</tiles:putAttribute>

</tiles:insertDefinition>