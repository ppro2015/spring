<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>

<tiles:insertDefinition name="superUserTemplate">
<tiles:putAttribute name="title" value="Edit word" />
<tiles:putAttribute name="body">

	<h2>Edit word <i>${word.key}</i></h2>
	<form:form method="POST" modelAttribute="word">
		<div class="row" style="margin: 15px;">
			<div class="col-md-7 col-xs-12">
				<tag:inputField name="key" label="Word" type="text"/>
				<tag:inputField name="answer" label="Answer" type="text"/>
				<button type="submit" class="btn btn-lg btn-primary">Update</button>
			</div>
		</div>
	</form:form>
	<br/>
	<br/>
	<a href="<c:url value='/test/${word.test.id_test}/words' />">Go back to list of all words</a>

</tiles:putAttribute>
</tiles:insertDefinition>