<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>

<tiles:insertDefinition name="superUserTemplate">
<tiles:putAttribute name="title" value="Delete word" />
<tiles:putAttribute name="body">

	<h2>Delete word</h2>

	<p>Are you sure you want to completely delete word <i>${word.key}</i>?</p>
	<form:form method="POST">
		<button type="submit" class="btn btn-lg btn-warning">Delete</button>
	</form:form>
	<br/>
	<br/>
	<a href="<c:url value='/test/${word.test.id_test}/words' />">Go back to list of all words of test <i>${word.test.title}</i></a>

</tiles:putAttribute>
</tiles:insertDefinition>