<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>

<tiles:insertDefinition name="mainTemplate">
<tiles:putAttribute name="title" value="All courses" />
<tiles:putAttribute name="body">

	<h2 class="pull-left">All courses</h2>
	<a href="<c:url value='/courses/create' />" class="pull-right btn btn-lg btn-primary">Create Course <span class="glyphicon glyphicon-education"></span></a>
	<div class="clearfix"></div>
	<div class="row">
		<div class="categories-menu col-md-3 col-sm-3">
			<c:forEach items="${categories}" var="category">
				<a class="category-item" href="<c:url value='/courses/category/${category.url}' />">${category.title}</a>
			</c:forEach>
		</div>
		<div class="row col-md-9 col-sm-9">
			<%@ include file="includes/showTests.jsp" %>
		</div>
	</div>

</tiles:putAttribute>
</tiles:insertDefinition>