<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>

<tiles:insertDefinition name="mainTemplate">
<tiles:putAttribute name="title" value="Review test" />
<tiles:putAttribute name="body">

<div class="review-page">
	<div id="test" class="row text-center">
		<div id="key-word" class="col-md-6 col-md-offset-3"></div>
		<div class="pull-right" id="word-counter"></div>
		<div class="clearfix"></div>
		<div class="col-md-6 col-md-offset-3">
			<input type="text" id="answer" class="form-control">
		</div>
		<div class="clearfix"></div>
		<div id="correct-answer" class="col-md-4 col-md-offset-4"></div>
		<div class="clearfix"></div>
		<div class="col-md-4 col-md-offset-4">
			<button id="next-button" class="btn btn-info btn-lg glyphicon glyphicon-menu-right" type="button">Next</button>
		</div>
	</div>
	<div id="loading">
		Loading, please wait
	</div>
	<p class="back-to-course"><a href="<c:url value='/courses/detail/${test.id_test}' />">Back to course</a>
	<table class="table table-nobordered"
		id="table-review"
		data-toggle="table"
		data-mobile-responsive="true">
		<thead>
		<tr>
			<th data-field="points">Points</th>
			<th data-field="correctAnswer">Word</th>
			<th data-field="accuracy" data-formatter="CourseReviewTable.accuracyFormatter">Accuracy</th>
			<th data-field="repetition_row" data-cell-style="CourseReviewTable.strikeCellStyle">Strike</th>
		</tr>
		</thead>
	<tbody></tbody>
	</table>
</div>
</tiles:putAttribute>

<tiles:putAttribute name="custom-scripts">
	<%@include file="reviewJs.jsp" %>
</tiles:putAttribute>

</tiles:insertDefinition>
