<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:forEach items="${courses}" var="course" varStatus="loop">
	<div class="course-wrapper col-md-4 col-sm-6 col-xs-6">
		<div class="course-box">
			<div class="course-box-inner">
				<a class="course-image" href="<c:url value='/courses/detail/${course.id_test}' />">
					<img src="${course.picture}">
				</a>
				<span class="pull-left course-author">by <c:choose>
						<c:when test="${course.author != null}">
							${course.author.nickname}
						</c:when>
						<c:otherwise>
							<i>-</i>
						</c:otherwise>
				</c:choose></span>
				<span class="pull-right course-category">
					<a href="<c:url value='/courses/category/${course.category.url}' />">${course.category.title}</a>
				</span>
				<div class="clearfix"></div>
				<a class="course-title" href="<c:url value='/courses/detail/${course.id_test}' />">${course.title}</a>
				<div class="course-users-count glyphicon glyphicon-user"> ${course.userCount}</div>
			</div>
		</div>
	</div>
	<c:if test="${loop.count % 3 == 0}">
		<div class="clearfix visible-md-block visible-lg-block"></div>
	</c:if>
	<c:if test="${loop.count % 2 == 0}">
		<div class="clearfix visible-sm-block"></div>
	</c:if>
</c:forEach>