<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script>
var CourseReviewClass = function() {
	var item,//current item
		count,//count of current item
		items,//loaded items for review
		maxCount,//number of reviewing items
		checkedItemShown = false,
		reviewData = [];//data for final table

	return {
		constructor: CourseReviewClass,
		init: init,
		nextItem: nextItem,
		checkAnswer: checkAnswer,
		showCheckedAnswer: showCheckedAnswer,
		showError: showError,
		documentKeyUp: documentKeyUp,
		redirect: redirect
	}

	/**
	 * Initialize variables and page after loading of data
	 * @param {Object} data words for review
	 */
	function init(data) {
		items = data;
		count = 0;
		maxCount = items.length;
		$("#key-word").show();
		$("#answer").show();
		$("#loading").hide();
	}

	/**
	 * Changes page so the user can type next answer
	 * or it shows final review if all words are answered
	 */
	function nextItem() {
		checkedItemShown = false;
		if (count < maxCount) {
			item = items[count];
			count++;
			$("#word-counter").html(count + " / " + maxCount); 
			$("#key-word").text(item.key);
			$("#correct-answer").hide().text("");
			$("#next-button").hide();
			$("#answer").prop("disabled", false)
						.removeClass("correct")
						.removeClass("incorrect")
						.val("")
						.focus();
		} else {
			$(".back-to-course").show();
			$("#test").hide();
			$("#table-review").show().bootstrapTable('append', reviewData);
		}
	}

	/**
	 * Checks answer thru AJAX
	 */
	function checkAnswer() {
		var answerVal = $("#answer").prop("disabled", true).val();
		$.get("<c:url value='/courses/review/${test.id_test}/check-word' />",
			{ answer: answerVal, id_word: item.id },
			function(data) {
				showCheckedAnswer(data);
		}).fail(function(data) {
			if (data.status == 400) {
				showError(data.responseText);
			} else {
				showError();
			}
		});
	}

	/**
	 * After word is checked with database via AJAX, this function changes page depending on the answer correctness
	 * @param {Object} data data about checked word
	 */
	function showCheckedAnswer(data) {
		reviewData.push(data);
		if (data.correct) {
			$("#answer").addClass("correct");
		} else {
			$("#answer").addClass("incorrect");
			$("#correct-answer").show().html("Correct answer: <b>" + data.correctAnswer + "</b>");
		}
		$("#next-button").show();
		checkedItemShown = true;
	}

	/**
	 * Just shows error when something goes wrong and then redirects
	 * @param {String} msg error message
	 */
	function showError(msg) {
		if (msg === undefined) {
			msg = "Sorry. Something went wrong.\nOur staff was notified and we are working on it!\nPlease try it later again.";
		}
		swal({
			title: "Error!",
			text: msg,
			type: "error"
		}, function() {
			redirect();
		});
	}

	/**
	 * Function handling document key up event
	 * @param {} e 
	 * @return {boolean} true/false - if function performed any action or not
	 */
	function documentKeyUp(e) {
		if (e.which == 13 && checkedItemShown) {
			CourseReview.nextItem();
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Redirects to course detail page
	 */
	function redirect() {
		window.location.replace("<c:url value='/courses/detail/${test.id_test}' />");
	}
};
var CourseReview = new CourseReviewClass();

$(document).ready(function() {
	$.get("<c:url value='/courses/review/${test.id_test}/get-words' />", function(data) {
		if (data.length == 0) {
			CourseReview.redirect();
			return;
		}
		CourseReview.init(data);
		CourseReview.nextItem();
	}).fail(function(data) {
		if (data.status == 400) {
			CourseReview.showError(data.responseText);
		} else {
			CourseReview.showError();
		}
	});

	$("#answer").keyup(function answerKeyUp(e) {
		if (e.which == 13) {
			CourseReview.checkAnswer();
			return false;
		}
	});

	$(document).keyup(function documentKeyUp(e) {
		return CourseReview.documentKeyUp(e);
	});

	$("#next-button").click(function nextButtonClick() {
		CourseReview.nextItem();
	});
});

var CourseReviewTableClass = function() {
	return {
		accuracyFormatter: accuracyFormatter,
		strikeCellStyle: strikeCellStyle
	};

	function accuracyFormatter(value, row, index) {
		return row.repetition_correct + "/" + row.repetition_count;
	}

	function strikeCellStyle(value, row, index) {
		var color = (value < 0) ? "red" : "green";
		return {
			css: {"color": color}
		};
	}
};
var CourseReviewTable = new CourseReviewTableClass();
</script>