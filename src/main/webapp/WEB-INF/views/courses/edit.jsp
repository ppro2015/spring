<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>

<tiles:insertDefinition name="mainTemplate">
<tiles:putAttribute name="title" value="Edit course" />
<tiles:putAttribute name="body">

	<div class="row" style="margin: 15px;">
		<div class="col-sm-6 col-xs-12">
			<c:choose>
				<c:when test="${testEntity != null}"><%-- když se edituje, tak zobrazit ten persistentní název, viz kontroler --%>
					<h2>Edit course <i>${testEntity.title}</i></h2>
				</c:when>
				<c:otherwise>
					<h2>Edit course <i>${test.title}</i></h2>
				</c:otherwise>
			</c:choose>
			<c:url var="edit_course_url" value="/courses/edit/${test.id_test}/test" />
			<form:form method="POST" action="${edit_course_url}" modelAttribute="test">
				<tag:inputField name="title" label="Title" type="text" />
				<tag:inputField name="description" label="Description" type="textarea" />
				<tag:inputField name="picture" label="Picture url" type="text" />
				<tag:inputField name="category.id_category" label="Category" type="select" data="${categories}" />
				<button type="submit" class="btn btn-lg btn-primary">Update course</button>
			</form:form>
			<br>
			<button class="btn btn-lg btn-danger ajax-delete-test">Delete course</button>
		</div>
		<div class="col-sm-6 col-xs-12">
			<h2>Add new word</h2>
			<c:url var="add_word_url" value="/courses/edit/${test.id_test}/new-word" />
			<form:form method="POST" action="${add_word_url}" modelAttribute="word">
				<tag:inputField name="key" label="Description" type="text"/>
				<tag:inputField name="answer" label="Answer" type="text"/>
				<button type="submit" class="btn btn-lg btn-primary">Create word</button>
			</form:form>
			<br>
			<h2>Edit words</h2>
			<table class="table table-bordered"
				id="table"
				data-toggle="table"
				data-unique-id="id"
				data-pagination="false">
			<thead>
			<tr>
				<th data-field="id" class="hidden">#</th>
				<th data-field="key">Key</th>
				<th data-field="answer">Answer</th>
				<th data-field="edit" class="text-center">&nbsp;</th>
				<th data-field="delete" class="text-center">&nbsp;</th>
			</tr>
			</thead>
			<tbody>
				<c:forEach items="${words}" var="w">
					<tr>
						<td>${w.id_word}</td>
						<td>${w.key}</td>
						<td>${w.answer}</td>
						<td><a class="btn btn-primary btn-xs ajax-edit-word ajax-href">
								<span class="glyphicon glyphicon-pencil"></span>
						</a></td>
						<td><a class="btn btn-danger btn-xs ajax-delete-word ajax-href">
								<span class="glyphicon glyphicon-trash"></span>
						</a></td>
					</tr>
				</c:forEach>
			</tbody>
			</table>
			<br>
			<div style="display: none" id="edit-word">
				<span class="glyphicon glyphicon-menu-up" style="cursor: pointer"></span>
				<h2>Edit word</h2>				
				<div class="row form-group">
					<div class="col-xs-12">
						<div class="form-group float-label-control">
							<label for="key">Key</label>
							<input id="key" class="form-control" placeholder="Key"/>
						</div>
					</div>
				</div>
				<div class="row form-group">
					<div class="col-xs-12">
						<div class="form-group float-label-control">
							<label for="answer">Answer</label>
							<input id="answer" class="form-control" placeholder="Answer"/>
						</div>
					</div>
				</div>
				<button type="submit" class="btn btn-lg btn-primary">Update word</button>
			</div>
		</div>
	</div>

</tiles:putAttribute>
<tiles:putAttribute name="custom-scripts">
	<tag:ajaxDelete what_delete="word" a_class="ajax-delete-word" url="courses/edit/${test.id_test}/ajax-delete-word"/>
<script>
$(document).ready(function() {
	var url = "",
		rowId = 0;
	function updateWord() {
		var key = $("#edit-word #key").val();
		var answer = $("#edit-word #answer").val();
		$.get(url, {key: key, answer: answer}, function(data) {
			$("table.table").bootstrapTable('updateByUniqueId', {
                id: rowId,
                row: {
                	key: key,
                	answer: answer
                }
            });
			$("#edit-word").slideUp(300);
			swal({
				title: "Updated!",
				type: "success"
			});
		}).fail(function(msg) {
			if (msg.responseText != "") {
				swal({
					title: "Error! " + msg.status,
					text: msg.responseText,
					type: "error"
				});
			} else {
				swal({
					title: "Error! " + msg.status,
					text: "Sorry. Something went wrong. That's all we know.",
					type: "error"
				});
			}
		});
	}

	$("#edit-word button").click(function() {
		updateWord();
	});

	$("#edit-word .glyphicon-menu-up").click(function() {
		$("#edit-word").slideUp(300);
	});

	function initAjaxEdit() {
		$(".ajax-edit-word").click(function() {
			var $answer = $(this).parent().prev();
			var answer = $answer.text();
			var key = $answer.prev().text();

			if ($("#edit-word").is(":visible")) {
				$("#edit-word").fadeOut(200, function() {
					$("#edit-word #key").val(key);
					$("#edit-word #answer").val(answer);
					$("#edit-word").fadeIn(200);
				});
			} else {
				$("#edit-word #key").val(key);
				$("#edit-word #answer").val(answer);
				$("#edit-word").slideDown(300);
			}

			var baseUrl = '<c:url value="/courses/edit/${test.id_test}/ajax-edit-word/" />';
			rowId = $answer.prev().prev().text();
			url = baseUrl + rowId;
		});
	}
	initAjaxEdit();
	$('#table').on('reset-view.bs.table', function () {
		initAjaxEdit();
	});

	$(".ajax-delete-test").click(function() {
		swal({
			title: "Delete test",
			text: "Are you sure you want to delete this test?",
			type: "warning",
			showCancelButton: true,
			closeOnConfirm: false,
			showLoaderOnConfirm: true, 
		}, function() {
			$.get("/SpringProject/courses/edit/${test.id_test}/ajax-delete", function(data) {
				swal({
					title: "Deleted!",
					type: "success"
				}, function() {
					window.location = '<c:url value="/courses/" />';
				});
			}).fail(function(msg) {
				swal({
					title: "Error! " + msg.status,
					text: "Sorry. Something went wrong. That's all we know.",
					type: "error"
				});
			});
		});
		return false;
	});
});
</script>
</tiles:putAttribute>
</tiles:insertDefinition>
