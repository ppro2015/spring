<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>

<tiles:insertDefinition name="mainTemplate">
<tiles:putAttribute name="title" value="${test.title}" />
<tiles:putAttribute name="body">

<div class="course-detail">
	<div class="row">
		<img class="col-sm-3 hidden-xs" src="${test.picture}">
		<div class="col-sm-9 col-xs-12">
			<c:if test="${user.id_user == test.author.id_user}">
				<a href="<c:url value='/courses/edit/${test.id_test}' />" class="pull-right btn btn-lg btn-primary">Edit Course <span class="glyphicon glyphicon-education"></span></a>
			</c:if>
			<div><a href="<c:url value='/courses/' />">Courses</a>
				<c:if test="${test.category.id_parent != null}"> > <a href="<c:url value='/courses/category/${test.category.id_parent.url}' />">${test.category.id_parent.title}</a></c:if>
				 > ${test.category.title}
			</div>
			<h2>${test.title}</h2>
			<p>${test.description}</p>
			<p>Created by ${test.author.nickname}</p>
			<c:choose>
				<c:when test="${!userInTest}">
					<a class="btn btn-primary btn-lg enroll-button" href="<c:url value='/courses/enroll/${test.id_test}' />">Start learning</a>
				</c:when>
				<c:when test="${somethingToReview}">
					<a class="btn btn-info btn-lg review-button" href="<c:url value='/courses/review/${test.id_test}' />">Review words</a>
				</c:when>
				<c:otherwise>
					<a class="btn btn-info btn-lg review-button disabled">Review words</a>
				</c:otherwise>
			</c:choose>
		</div>
	</div>
	<c:choose>
		<c:when test="${userInTest}">
			<table class="table table-bordered"
				id="table"
				data-toggle="table"
				data-pagination="false">
			<thead>
			<tr>
				<th data-field="key"></th>
				<th data-field="answer" class="bold"></th>
				<th data-field="next_repetition"></th>
			</tr>
			</thead>
			<tbody>
				<c:forEach items="${userWords}" var="userWord">
						<tr>
						<td>${userWord.word.key}</td>
						<td>${userWord.word.answer}</td>
						<td>${userWord.nextRepetitionString}</td>
					</tr>
				</c:forEach>
			</tbody>
			</table>
		</c:when>
		<c:otherwise>
			<table class="table table-bordered"
				id="table"
				data-toggle="table"
				data-pagination="false">
			<thead>
			<tr>
				<th data-field="key"></th>
				<th data-field="answer" class="bold"></th>
			</tr>
			</thead>
			<tbody>
				<c:forEach items="${words}" var="word">
						<tr>
						<td>${word.key}</td>
						<td>${word.answer}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</c:otherwise>
	</c:choose>
</div>
</tiles:putAttribute>
</tiles:insertDefinition>
