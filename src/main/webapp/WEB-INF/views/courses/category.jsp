<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>

<tiles:insertDefinition name="mainTemplate">
<tiles:putAttribute name="title" value="${current_category.title} courses" />
<tiles:putAttribute name="body">

	<h2 class="pull-left">Courses in ${current_category.title}</h2>
	<a href="<c:url value='/courses/create' />" class="pull-right btn btn-lg btn-primary">Create Course <span class="glyphicon glyphicon-education"></span></a>
	<div class="clearfix"></div>
	<div class="row">
	<div class="categories-menu col-md-3 col-sm-3">
	<c:forEach items="${categories}" var="category">
		<c:choose>
			<c:when test="${current_category.id_category == category.id_category}">
				<a class="active category-item" href="<c:url value='/courses/category/${category.url}' />">${category.title}</a>
			</c:when>	
			<c:otherwise>
				 <a class="category-item" href="<c:url value='/courses/category/${category.url}' />">${category.title}</a>
			</c:otherwise>
		</c:choose>
	</c:forEach>
	</div>
	<div class="row col-md-9 col-sm-9">
		<c:choose>
			<c:when test="${fn:length(courses) == 0}">
				<h3 class="text-center">No courses found in this category.</h3>
			</c:when>	
			<c:otherwise>
				<%@ include file="includes/showTests.jsp" %>
			</c:otherwise>
		</c:choose>
	</div>
	</div>
</tiles:putAttribute>

</tiles:insertDefinition>