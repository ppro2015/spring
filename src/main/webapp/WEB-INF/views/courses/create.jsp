<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>

<tiles:insertDefinition name="mainTemplate">
<tiles:putAttribute name="title" value="New course" />
<tiles:putAttribute name="body">

	<h2>New Course</h2>
	<form:form method="POST" modelAttribute="test">
		<div class="row" style="margin: 15px;">
			<div class="col-md-7 col-xs-12">
				<tag:inputField name="title" label="Title" type="text" />
				<tag:inputField name="description" label="Description" type="textarea" />
				<tag:inputField name="picture" label="Picture url" type="text" />
				<tag:inputField name="category.id_category" label="Category" type="select" data="${categories}" />
				<button type="submit" class="btn btn-lg btn-primary">Create</button>
			</div>
		</div>
	</form:form>

</tiles:putAttribute>
</tiles:insertDefinition>
