<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>

<tiles:insertDefinition name="superUserTemplate">
<tiles:putAttribute name="title" value="List of all words" />
<tiles:putAttribute name="body">

	<h2>List of all words of user <i>${user.nickname}</i></h2>
	<p><a href="<c:url value='/users/' />">Go back to list of all users</a></p>
	<noscript>Found ${fn:length(words)} words.</noscript>

	<jsp:include page="../table-settings.jsp" />
	<thead>
	<tr>
		<th data-field="id" data-sortable="true">#</th>
		<th data-field="key" data-sortable="true">Key</th>
		<th data-field="answer" data-sortable="true">Answer</th>
		<th data-field="test" data-sortable="true">Test</th>
		<th data-field="words" data-sortable="true">Test's words</th>
	</tr>
	</thead>
	<tbody>
		<c:forEach items="${userWords}" var="userWord">
			<tr>
				<td>${userWord.word.id_word}</td>
				<td>${userWord.word.key}</td>
				<td>${userWord.word.answer}</td>
				<td><a href="<c:url value='/tests/edit/${userWord.word.test.id_test}' />">${userWord.word.test.title}</a></td>
				<td><a href="<c:url value='/test/${userWord.word.test.id_test}/words' />">Manage test's words</a></td>
			</tr>
		</c:forEach>
	</tbody>
	</table>
</tiles:putAttribute>

<tiles:putAttribute name="custom-scripts">
	<tag:tooltipInit/>
</tiles:putAttribute>

</tiles:insertDefinition>