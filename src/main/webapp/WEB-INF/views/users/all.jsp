<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>

<tiles:insertDefinition name="superUserTemplate">
<tiles:putAttribute name="title" value="List of users" />
<tiles:putAttribute name="body">

	<h2>List of all users</h2>
	<p><a href="<c:url value='/users/new' />">Add New User</a></p>
	<noscript><p>Found ${fn:length(users)} users.</p></noscript>

	<jsp:include page="../table-settings.jsp" />
	<thead>
	<tr>
		<th data-field="id" data-sortable="true" class="hidden-md hidden-sm hidden-xs">#</th>
		<th data-field="nickname" data-sortable="true">Nickname</th>
		<th data-field="role" data-sortable="true"class="hidden-xs">Role</th>
		<th data-field="registration_date" data-sortable="true" data-sorter="dateTimeSorter" class="hidden-md hidden-sm hidden-xs">Registration date</th>
		<th data-field="daily_goal" data-sortable="true" class="hidden-sm hidden-xs">Daily goal</th>
		<th data-field="daily_done" data-sortable="true" class="hidden-md hidden-sm hidden-xs">Points done today</th>
		<th data-field="points" data-sortable="true">Total points</th>
		<th data-searchable="false">Tests</th>
		<th data-searchable="false">Words</th>
		<th data-searchable="false">&nbsp;</th>
		<th data-searchable="false">&nbsp;</th>
		<th data-searchable="false">&nbsp;</th>
	</tr>
	</thead>
	<tbody>
		<c:forEach items="${users}" var="user">
			<tr>
			<td>${user.id_user}</td>
			<td>${user.nickname}</td>
			<td>${user.role}</td>
			<td><fmt:formatDate value="${user.registration_date}" pattern="dd.MM.yyyy HH:mm:ss"/></td>
			<td>${user.daily_goal}</td>
			<td>${user.daily_done}</td>
			<td>${user.points}</td>
			<td>${fn:length(user.userTests)}</td>
			<td><a href="<c:url value='/users/words/${user.id_user}' />">Words</a></td>
			<td><a data-placement="top" data-toggle="tooltip" title="Edit"
					class="btn btn-primary btn-xs" href="<c:url value='/users/edit/${user.id_user}' />">
					<span class="glyphicon glyphicon-pencil"></span>
			</a></td>
			<td><a data-placement="top" data-toggle="tooltip" title="Edit user's password"
					class="btn btn-primary btn-xs" href="<c:url value='/users/edit-password/${user.id_user}' />">
					<span class="glyphicon glyphicon-floppy-disk"></span>
			</a></td>
			<td><a data-placement="top" data-toggle="tooltip" title="Delete"
					class="btn btn-danger btn-xs ajax-delete-user" href="<c:url value='/users/delete/${user.id_user}' />">
					<span class="glyphicon glyphicon-trash"></span>
			</a></td>
			</tr>
		</c:forEach>
	</tbody>
	</table>
</tiles:putAttribute>

<tiles:putAttribute name="custom-scripts">
	<tag:ajaxDelete what_delete="user" a_class="ajax-delete-user" url="users/ajax-delete" />
	<tag:tooltipInit/>
<script>
function dateTimeSorter(a, b) {
	var year1 = a.substring(6, 10) * 1;
	var year2 = b.substring(6, 10) * 1;
	if (year1 > year2) return 1;
	else if (year1 < year2) return -1;
	else {
		var month1 = a.substring(3, 5) * 1;
		var month2 = b.substring(3, 5) * 1;
		if (month1 > month2) return 1;
		else if (month1 < month2) return -1;
		else {
			var day1 = a.substring(0, 2) * 1;
			var day2 = b.substring(0, 2) * 1;
			if (day1 > day2) return 1;
			else if (day1 < day2) return -1;
			else {
				var hour1 = a.substring(11, 13) * 1;
				var hour2 = b.substring(11, 13) * 1;
				if (hour1 > hour2) return 1;
				else if (hour1 < hour2) return -1;
				else {
					var minute1 = a.substring(14, 16) * 1;
					var minute2 = b.substring(14, 16) * 1;
					if (minute1 > minute2) return 1;
					else if (minute1 < minute2) return -1;
					else {
						var second1 = a.substring(17, 19) * 1;
						var second2 = b.substring(17, 19) * 1;
						if (second1 > second2) return 1;
						else if (second1 < second2) return -1;
						else {
							return 0;
						}
					}
				}
			}
		}
	}
}
</script>

</tiles:putAttribute>
</tiles:insertDefinition>