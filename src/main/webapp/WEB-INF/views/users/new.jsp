<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>

<tiles:insertDefinition name="superUserTemplate">
<tiles:putAttribute name="title" value="New user" />
<tiles:putAttribute name="body">

	<h2>New user</h2>
	<form:form method="POST" modelAttribute="user">
		<div class="row" style="margin: 15px;">
			<div class="col-md-7 col-xs-12">
				<tag:inputField name="nickname" label="Nickname" type="text"/>
				<tag:inputField name="password" label="Password" type="password"/>
				<tag:inputField name="confirmPassword" label="Confirm Password" type="password"/>
				<tag:inputField name="role" label="Role" type="select"/>
				<button type="submit" class="btn btn-lg btn-primary">Register</button>
			</div>
		</div>
	</form:form>
	<br/>
	<br/>
	<a href="<c:url value='/users/' />">Go back to list of all users</a>

</tiles:putAttribute>
</tiles:insertDefinition>