<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>

<tiles:insertDefinition name="superUserTemplate">
<tiles:putAttribute name="title" value="New test" />
<tiles:putAttribute name="body">

	<h2>New test</h2>
	<form:form method="POST" modelAttribute="test">
		<div class="row" style="margin: 15px;">
			<div class="col-md-7 col-xs-12">
				<tag:inputField name="title" label="Title" type="text" />
				<tag:inputField name="description" label="Description" type="textarea" />
				<tag:inputField name="picture" label="Picture url (400px x 400px)" type="text" />
				<tag:inputField name="author.id_user" label="Author" type="select" data="${users}" />
				<tag:inputField name="category.id_category" label="Category" type="select" data="${categories}" />
				<button type="submit" class="btn btn-lg btn-primary">Create</button>
			</div>
		</div>
	</form:form>
	<br/>
	<br/>
	<a href="<c:url value='/tests/' />">Go back to list of all tests</a>

</tiles:putAttribute>
</tiles:insertDefinition>
