<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>
<% pageContext.setAttribute("newLineChar", "\n"); %> 

<tiles:insertDefinition name="superUserTemplate">
<tiles:putAttribute name="title" value="List of all tests" />
<tiles:putAttribute name="body">

	<h2>List of all tests</h2>
	<p><a href="<c:url value='/tests/new' />">Add new test</a></p>

	<jsp:include page="../table-settings.jsp" />
	<thead>
	<tr>
		<th data-field="id" data-sortable="true">#</th>
		<th data-field="title" data-sortable="true">Title</th>
		<th data-field="description" data-sortable="true" class="hidden-sm hidden-xs">Description</th>
		<th data-searchable="false">Words</th>
		<th data-field="author" data-sortable="true">Author</th>
		<th data-field="category" data-sortable="true">Category</th>
		<th data-field="users" data-sortable="true">Users</th>
		<th data-searchable="false">&nbsp;</th>
		<th data-searchable="false">&nbsp;</th>
	</tr>
	</thead>
	<tbody>
		<c:forEach items="${tests}" var="test">
			<tr>
			<td>${test.id_test}</td>
			<td>${test.title}</td>
			<td>${fn:replace(test.description, newLineChar, "<br>")}</td>
			<td><a href="<c:url value='/test/${test.id_test}/words' />">Words</a></td>
			<td><c:choose>
				<c:when test="${test.author != null}">
					<a href="<c:url value='/users/edit/${test.author.id_user}' />">${test.author.nickname}</a>
				</c:when>
				<c:otherwise>
					<i>Deleted user</i>
				</c:otherwise>
			</c:choose></td>
			<td><a href="<c:url value='/categories/edit/${test.category.id_category}' />">${test.category.title}</a></td>
			<td><a href="<c:url value='/test/${test.id_test}/users' />">${fn:length(test.userTests)}</a></td>

			<td><a data-placement="top" data-toggle="tooltip" title="Edit"
					class="btn btn-primary btn-xs" href="<c:url value='/tests/edit/${test.id_test}' />">
					<span class="glyphicon glyphicon-pencil"></span>
			</a></td>
			<td><a data-placement="top" data-toggle="tooltip" title="Delete"
					class="btn btn-danger btn-xs ajax-delete-test" href="<c:url value='/tests/delete/${test.id_test}' />">
					<span class="glyphicon glyphicon-trash"></span>
			</a></td>
			</tr>
		</c:forEach>
	</tbody>
	</table>
</tiles:putAttribute>

<tiles:putAttribute name="custom-scripts">
	<tag:ajaxDelete what_delete="test" a_class="ajax-delete-test" url="tests/ajax-delete"/>
	<tag:tooltipInit/>
</tiles:putAttribute>

</tiles:insertDefinition>